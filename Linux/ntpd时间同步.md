```
# cat /etc/ntp.conf
driftfile /var/lib/ntp/drift
pidfile /var/run/ntpd.pid
logfile /var/log/ntp.log

# Access Control Support
restrict default ignore
restrict -6 default ignore
restrict 127.0.0.1

# Local clock
server 127.127.1.0
fudge 127.127.1.0 stratum 10

# Server list
server ntp.sdns.ksyun.com iburst prefer minpoll 5 maxpoll 10
server ntp1.sdns.ksyun.com iburst minpoll 5 maxpoll 10
server ntp2.sdns.ksyun.com iburst minpoll 5 maxpoll 10
server ntp3.sdns.ksyun.com iburst minpoll 5 maxpoll 10

#End
```
```
# timedatectl
      Local time: Wed 2019-09-11 20:11:38 CST
  Universal time: Wed 2019-09-11 12:11:38 UTC
        RTC time: Wed 2019-09-11 12:11:37
       Time zone: Asia/Shanghai (CST, +0800)
     NTP enabled: yes
NTP synchronized: yes
 RTC in local TZ: no
      DST active: n/a
      
# ntpdate ntp.sdns.ksyun.com
11 Sep 20:12:36 ntpdate[23110]: the NTP socket is in use, exiting

# lsof -i:123
COMMAND   PID USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
ntpd    23082  ntp   16u  IPv4  77582      0t0  UDP *:ntp 
ntpd    23082  ntp   17u  IPv6  77583      0t0  UDP *:ntp 
ntpd    23082  ntp   18u  IPv4  77588      0t0  UDP localhost:ntp 
ntpd    23082  ntp   19u  IPv4  77589      0t0  UDP bjwg01-dragonbase-005.bjwg01.ksyun.com:ntp 
ntpd    23082  ntp   20u  IPv6  77590      0t0  UDP bjwg01-dragonbase-005.bjwg01.ksyun.com:ntp 
ntpd    23082  ntp   21u  IPv6  77591      0t0  UDP localhost:ntp 
# kill -9 23082

 ntpdate ntp1.sdns.ksyun.com
11 Sep 12:43:03 ...
```
```
hwclock -w
```