```
# 把本机的公钥追到140的 .ssh/authorized_keys
ssh-copy-id -i ~/.ssh/id_rsa.pub dragon@10.120.1.140
```
```
# ssh-copy-id not working Permission denied (publickey).
Log in as root
Edit ssh config: 
sudo nano /etc/ssh/sshd_config
Change this line:
PasswordAuthentication no
to
PasswordAuthentication yes
Restart daemon: 
sudo systemctl restart sshd
```
```
[dragon@bjwg01-dragonbase-001 ~]$ ssh 10.120.1.140 date
Mon Sep  2 10:14:56 CST 2019
```
```
$ cat ssh-copy-id.sh 
#!/usr/bin/expect

foreach ip {10.0.0.4 10.0.0.8 10.0.0.16 10.0.0.6 10.0.0.11} {
	spawn ssh-copy-id -i /home/dragon/.ssh/id_rsa.pub dragon@$ip
	expect {
		"(yes/no)?"
		{
			send "yes\n"
			expect "*assword:" { send "dragon123\n"}
		}
		"*assword:"
		{
			send "dragon123\n"
		}
	}
	exec sleep 1
}

foreach ip {10.0.0.4 10.0.0.8 10.0.0.16 10.0.0.6 10.0.0.11} {
	spawn ssh dragon@$ip date
	exec sleep 1
}
```

