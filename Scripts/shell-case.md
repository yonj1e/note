

## 文件序号递增

```shell
#!/bin/bash
# ----------------------------------------
# 打包文件，并且序号递增
# ----------------------------------------

FILE=highgo

var=`ls *\`date '+%Y%m%d'\`*|wc -l`
let var+=1

tar zcvf ${FILE}.`date '+%Y%m%d'`-${var}.tar.gz ${FILE}
```



## sed 替换路径

```shell
#!/bin/bash

OLD_VARPATH="/opt/HighGo/db"
NEW_VARPATH="/work/hgdb/hgdb5"
OLD_VARPATHSED=$(echo ${OLD_VARPATH} |sed -e 's/\//\\\//g' )
NEW_VARPATHSED=$(echo ${NEW_VARPATH} |sed -e 's/\//\\\//g' )

sed -i "s/${OLD_VARPATHSED}/${NEW_VARPATHSED}/g" ./output.txt
```



## sed 替换某几行内容

```shell
num1=`sed -n -e '/# TYPE  DATABASE/=' $data/pg_hba.conf`
num2=`sed -n '$=' $data/pg_hba.conf`
sed -i "${num1},${num2}s/trust/md5/g" $data/pg_hba.conf
sed -i "${num1},${num2}s/127.0.0.1\/32/0.0.0.0\/0/g" $data/pg_hba.conf
```



## 脚本报错立即退出

```shell
#!/bin/bash

set -o errexit
```



## 多行注释

```shell
#!/bin/bash

:<<EOF
# Multiline comment
# Multiline comment
# Multiline comment
EOF
```

