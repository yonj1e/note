Expect 脚本语言用于自动提交输入到交互程序。

它的工作原理是等待特定字符串，并发送或响应相应的字符串。

以下三个expect命令用于任何自动化互动的过程。

- send – 发送字符串到进程
- expect – 等待来自进程的特定的字符串
- spawn – 启动命令 

请确保在您的系统上安装expect软件包，因为它不会被默认安装。 

## ssh

```shell
#!/usr/bin/expect

set timeout 20
set ip [lindex $argv 0]
set user [lindex $argv 1]
set password [lindex $argv 2]

#set user young
#set password pa4word

 spawn ssh $user@$ip  
 expect {  
     "*yes/no" { send "yes\r"; exp_continue }
     "*password:" { send "$password\r" }
 } 
 
interact
```

## scp

```shell
#!/usr/bin/expect

set timeout 300
set host [lindex $argv 0]
set username [lindex $argv 1]
set password [lindex $argv 2]
set src_file [lindex $argv 3]
set dest_file [lindex $argv 4]

spawn scp $src_file $username@$host:$dest_file
expect {  
	"*yes/no" { send "yes\r"; exp_continue }
	"*password:" { send "$password\r" }
} 

# 行完成后保持交互状态，把控制权交给控制台，这个时候就可以手工操作了。
# 果没有这一句登录完成后会退出，而不是留在远程终端上。
# 如果你只是登录过去执行一段命令就退出，可改为［expect eof］
#interact
expect "100%"
expect eof
```

