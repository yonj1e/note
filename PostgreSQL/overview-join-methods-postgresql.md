---
title: PostgreSQL中JOIN方法的概述
date: 2019-09-06
categories: 
  - [PostgreSQL]
tags: 
  - JOIN
---

https://severalnines.com/database-blog/overview-join-methods-postgresql

In [my previous blog](https://severalnines.com/database-blog/overview-various-scan-methods-postgresql), we discussed various ways to select, or scan, data from a single table. But in practical, fetching data from a single table is not enough. It  requires selecting data from multiple tables and then correlating among  them. Correlation of this data among tables is called joining tables and it can be done in various ways. As the joining of tables requires input data (e.g. from the table scan), it can never be a leaf node in the  plan generated.

在我以前的博客中，我们讨论了从单个表中选择或扫描数据的各种方法。但是实际上，仅从单个表中获取数据是不够的。它需要从多个表中选择数据，然后在它们之间进行关联。表之间此数据的关联称为联接表，可以通过多种方式进行关联。由于表的联接需要输入数据（例如，来自表扫描的数据），因此它永远不可能是生成的计划中的叶节点。

E.g. consider a simple query example as SELECT * FROM TBL1, TBL2 where TBL1.ID > TBL2.ID; and suppose the plan generated is as below:

例如，考虑一个简单的查询示例：`SELECT * FROM TBL1, TBL2 where TBL1.ID > TBL2.ID;` 并假设生成的计划如下：

![Nested Loop Join](overview-join-methods-postgresql/image2.png)

So here the first both tables are scanned and then they are joined together as per the correlation condition as TBL1.ID > TBL2.ID

因此，这里首先扫描两个表，然后根据相关条件将它们连接在一起，如：TBL1.ID > TBL2.ID

In addition to the join method, the join order is also very important. Consider the below example:

除了连接方法外，连接顺序也很重要。考虑以下示例：

```sql
SELECT * FROM TBL1, TBL2, TBL3 WHERE TBL1.ID=TBL2.ID AND TBL2.ID=TBL3.ID;
```

Consider that TBL1, TBL2 AND TBL3 have 10, 100 and 1000 records respectively. 

考虑到TBL1，TBL2和TBL3分别具有10、100和1000条记录。 

The condition TBL1.ID=TBL2.ID returns only 5 records, whereas TBL2.ID=TBL3.ID returns 100 records, then it’s better to join TBL1 and TBL2 first so that lesser number of records get joined with TBL3. The plan will be as shown below:

条件TBL1.ID = TBL2.ID仅返回5条记录，而TBL2.ID = TBL3.ID返回100条记录，那么最好先加入TBL1和TBL2，以便较少的记录数与TBL3结合。该计划将如下所示：

![Nested Loop Join with Table Order](overview-join-methods-postgresql/image1.png)

PostgreSQL supports the below kind of joins:

- Nested Loop Join
- Hash Join
- Merge Join

Each of these Join methods are equally useful depending on the query and  other parameters e.g. query, table data, join clause, selectivity,  memory etc. These join methods are implemented by most of the relational databases.

根据查询和其他参数（例如查询，表数据，join子句，选择性，内存等），这些Join方法中的每一个都是同等有用的。这些join方法由大多数关系数据库实现。 

Let’s create some pre-setup table and populate with some data, which will be  used frequently to better explain these scan methods.

让我们创建一些预设置表并填充一些数据，这些数据将经常用于更好地解释这些扫描方法。 

```sql
postgres=# create table blogtable1(id1 int, id2 int);
CREATE TABLE
postgres=# create table blogtable2(id1 int, id2 int);
CREATE TABLE
postgres=# insert into blogtable1 values(generate_series(1,10000),3);
INSERT 0 10000
postgres=# insert into blogtable2 values(generate_series(1,1000),3);
INSERT 0 1000
postgres=# analyze;
ANALYZE
```

In all our subsequent examples, we consider default configuration parameter unless otherwise specified specifically.

在所有后续示例中，除非特别说明，否则我们将考虑默认配置参数。 

## Nested Loop Join

Nested Loop Join (NLJ) is the simplest join algorithm wherein each record of  outer relation is matched with each record of inner relation. The Join  between relation A and B with condition A.ID < B.ID can be  represented as below:

嵌套循环连接（NLJ）是最简单的连接算法，其中外部关系的每个记录都与内部关系的每个记录匹配。条件为A.ID <B.ID的关系A和B之间的连接可以表示如下：

```sql
For each tuple r in A
        For each tuple s in B
                If (r.ID < s.ID)
                    Emit output tuple (r,s)
```

Nested Loop Join (NLJ) is the most common joining method and it can be used  almost on any dataset with any type of join clause. Since this algorithm scan all tuples of inner and outer relation, it is considered to be the most costly join operation.

嵌套循环连接（NLJ）是最常见的连接方法，几乎可以在具有任何类型的连接子句的任何数据集上使用。由于此算法扫描内部和外部关系的所有元组，因此被认为是最昂贵的联接操作。

As per the above table and data, the following query will result in a Nested Loop Join as shown below:

根据上表和数据，以下查询将导致嵌套循环联接，如下所示：

```sql
postgres=# explain select * from blogtable1 bt1, blogtable2 bt2 where bt1.id1 < bt2.id1;
                               QUERY PLAN
------------------------------------------------------------------------------
 Nested Loop  (cost=0.00..150162.50 rows=3333333 width=16)
   Join Filter: (bt1.id1 < bt2.id1)
   ->  Seq Scan on blogtable1 bt1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Materialize  (cost=0.00..20.00 rows=1000 width=8)
         ->  Seq Scan on blogtable2 bt2  (cost=0.00..15.00 rows=1000 width=8)
(5 rows)
```

Since the join clause is “<”, the only possible join method here is Nested Loop Join.

由于join子句为“ <”，因此这里唯一可能的联接方法是嵌套循环联接。

Notice here one new kind of node as Materialize; this node acts as intermediate result cache i.e. instead of fetching  all tuples of a relation multiple times, the first time fetched result  is stored in memory and on the next request to get tuple will be served  from the memory instead of fetching from the relation pages again.  In-case if all tuples cannot be fit in memory then spill-over tuples go  to a temporary file. It is mostly useful in-case of Nested Loop Join and to some extent in-case of Merge Join as they rely on rescan of inner  relation. Materialize Node is not only limited to caching result of  relation but it can cache results of any node below in the plan tree.  

在这里注意一种新的节点为Materialize ; 此节点充当中间结果缓存，即，不是将多次获取关系的所有元组，而是将第一次获取的结果存储在内存中，并且在下一次获取元组的请求上将从内存中提供服务，而不是再次从关系页中获取。万一如果所有元组都无法容纳在内存中，则溢出的元组将进入一个临时文件。这在嵌套循环联接的情况下最有用，而在合并联接的情况下则在某种程度上有用，因为它们依赖于内部关系的重新扫描。物化节点不仅限于关系的缓存结果，而且还可以缓存计划树下面任何节点的结果。

TIP: In case join clause is “=” and nested loop join is chosen between a  relation, then it is really important to investigate if more efficient  join method such as hash or merge join can be chosen by tuning  configuration (e.g. work_mem but not limited to ) or by adding an index, etc.

提示：如果join子句为“ =”，并且在关系之间选择了嵌套循环联接，那么研究是否可以通过调整配置选择更有效的联接方法（例如哈希或合并联接）非常重要（例如work_mem但不限于到）或添加索引等。

Some of the queries may not have join clause, in that case also the only  choice to join is Nested Loop Join. E.g. consider the below queries as  per the pre-setup data:

一些查询可能没有join子句，在这种情况下，联接的唯一选择是嵌套循环联接。例如，根据预设置数据考虑以下查询：

```sql
postgres=# explain select * from blogtable1, blogtable2;
                             QUERY PLAN
--------------------------------------------------------------------------
 Nested Loop  (cost=0.00..125162.50 rows=10000000 width=16)
   ->  Seq Scan on blogtable1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Materialize  (cost=0.00..20.00 rows=1000 width=8)
      ->  Seq Scan on blogtable2  (cost=0.00..15.00 rows=1000 width=8) 
(4 rows)
```

The join in the above example is just a Cartesian product of both tables.

上面示例中的联接只是两个表的笛卡尔乘积。 

## Hash Join

This algorithm works in two phases:

- Build Phase: A Hash table is built using the inner relation records. The hash key is calculated based on the join clause key.
- 构建阶段：使用内部关系记录构建哈希表。哈希键是基于join子句键计算的。
- Probe Phase: An outer relation record is hashed based on the join clause key to find matching entry in the hash table.
- 探测阶段：根据join子句键对外部关系记录进行哈希处理，以在哈希表中找到匹配的条目。 

The join between relation A and B with condition A.ID = B.ID can be represented as below:

条件为A.ID = B.ID的关系A和B之间的联接可以表示如下： 

- Build Phase
  - For each tuple r in inner relation B
  - Insert r into hash table HashTab with key r.ID
- Probe Phase
- For each tuple s in outer relation A
- For each tuple r in bucker HashTab[s.ID]
- If (s.ID = r.ID)
  - Emit output tuple (r,s)

As per above pre-setup table and data, the following query will result in a Hash Join as shown below:

根据上面的预设置表和数据，以下查询将导致哈希联接，如下所示： 

```sql
postgres=# explain select * from blogtable1 bt1, blogtable2 bt2 where bt1.id1 = bt2.id1;
                               QUERY PLAN
------------------------------------------------------------------------------
 Hash Join  (cost=27.50..220.00 rows=1000 width=16)
   Hash Cond: (bt1.id1 = bt2.id1)
   ->  Seq Scan on blogtable1 bt1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Hash  (cost=15.00..15.00 rows=1000 width=8)
         ->  Seq Scan on blogtable2 bt2  (cost=0.00..15.00 rows=1000 width=8) 
(5 rows)
```

Here the hash table is created on the table blogtable2 because it is the  smaller table so the minimal memory required for hash table and whole  hash table can fit in memory.

在这里，哈希表是在表blogtable2上创建的，因为它是较小的表，因此哈希表和整个哈希表所需的最小内存可以容纳在内存中。 

## Merge Join 

Merge Join is an algorithm wherein each record of outer relation is matched  with each record of inner relation until there is a possibility of join  clause matching. This join algorithm is only used if both relations are  sorted and join clause operator is “=”. The join between relation A and B with condition A.ID = B.ID can be represented as below:

合并连接是一种算法，其中外部关系的每个记录与内部关系的每个记录匹配，直到存在连接子句匹配的可能性为止。仅当两个关系都已排序且join子句运算符为“ =”时，才使用此join算法。条件为A.ID = B.ID的关系A和B之间的联接可以表示如下：

```sql
For each tuple r in A
    For each tuple s in B
         If (r.ID = s.ID)
              Emit output tuple (r,s)
              Break;
         If (r.ID > s.ID)
              Continue;
         Else
              Break;
```

The example query which resulted in a Hash Join, as shown above, can result in a Merge Join if the index gets created on both tables. This is  because the table data can be retrieved in sorted order because of the  index, which is one of the major criteria for the Merge Join method:

如上所示，导致在哈希联接中进行查询的示例查询可以在两个表上都创建索引时导致合并联接。这是因为由于索引，可以按排序的顺序检索表数据，而索引是合并联接方法的主要标准之一：

```sql
postgres=# create index idx1 on blogtable1(id1);
CREATE INDEX
postgres=# create index idx2 on blogtable2(id1);
CREATE INDEX
postgres=# explain select * from blogtable1 bt1, blogtable2 bt2 where bt1.id1 = bt2.id1;
                                   QUERY PLAN
---------------------------------------------------------------------------------------
 Merge Join  (cost=0.56..90.36 rows=1000 width=16)
   Merge Cond: (bt1.id1 = bt2.id1)
   ->  Index Scan using idx1 on blogtable1 bt1  (cost=0.29..318.29 rows=10000 width=8)
   ->  Index Scan using idx2 on blogtable2 bt2  (cost=0.28..43.27 rows=1000 width=8)
(4 rows)
```

So, as we see, both tables are using index scan instead of sequential scan  because of which both tables will emit sorted records.

因此，正如我们所看到的，两个表都使用索引扫描而不是顺序扫描，因为这两个表都会发出排序的记录。 

## Configuration

PostgreSQL supports various planner related configurations, which can be used to  hint the query optimizer to not select some particular kind of join  methods. If the join method chosen by the optimizer is not optimal, then these configuration parameters can be switch-off to force the query  optimizer to choose a different kind of join methods. All of these  configuration parameters are “on” by default. Below are the planner  configuration parameters specific to join methods.

PostgreSQL支持各种与计划程序相关的配置，这些配置可用于提示查询优化器不选择某些特定种类的联接方法。如果优化器选择的联接方法不是最佳的，则可以关闭这些配置参数以强制查询优化器选择其他类型的联接方法。默认情况下，所有这些配置参数都为“ on”。以下是特定于联接方法的计划程序配置参数。

- **enable_nestloop**: It corresponds to Nested Loop Join.
- **enable_hashjoin**: It corresponds to Hash Join.
- **enable_mergejoin**: It corresponds to Merge Join.

There are many plan related configuration parameters used for various  purposes. In this blog, keeping it restricted to only join methods.

有许多与计划相关的配置参数可用于各种目的。在此博客中，使其仅限于联接方法。

These parameters can be modified from a particular session. So in-case we  want to experiment with the plan from a particular session, then these  configuration parameters can be manipulated and other sessions will  still continue to work as it is.

可以从特定会话中修改这些参数。因此，万一我们想从特定会话中试验计划，那么可以操纵这些配置参数，而其他会话仍将按原样继续工作。

Now, consider the above examples of merge join and hash join. Without an  index, query optimizer selected a Hash Join for the below query as shown below but after using configuration, it switches to merge join even  without index:

现在，考虑以上合并联接和哈希联接的示例。没有索引，查询优化器为以下查询选择了哈希联接，如下所示，但是在使用配置后，即使没有索引，它也会切换为合并联接：

```sql
postgres=# explain select * from blogtable1, blogtable2 where blogtable1.id1 = blogtable2.id1;
                             QUERY PLAN
--------------------------------------------------------------------------
 Hash Join  (cost=27.50..220.00 rows=1000 width=16)
   Hash Cond: (blogtable1.id1 = blogtable2.id1)
   ->  Seq Scan on blogtable1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Hash  (cost=15.00..15.00 rows=1000 width=8)
      ->  Seq Scan on blogtable2  (cost=0.00..15.00 rows=1000 width=8) 
(5 rows)
 
postgres=# set enable_hashjoin to off;
SET
postgres=# explain select * from blogtable1, blogtable2 where blogtable1.id1 = blogtable2.id1;
                             QUERY PLAN
----------------------------------------------------------------------------
 Merge Join  (cost=874.21..894.21 rows=1000 width=16)
   Merge Cond: (blogtable1.id1 = blogtable2.id1)
   ->  Sort  (cost=809.39..834.39 rows=10000 width=8)
      Sort Key: blogtable1.id1
      ->  Seq Scan on blogtable1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Sort  (cost=64.83..67.33 rows=1000 width=8)
      Sort Key: blogtable2.id1
      ->  Seq Scan on blogtable2  (cost=0.00..15.00 rows=1000 width=8)
(8 rows)
```

Initially Hash Join is chosen because data from tables are not sorted. In order  to choose the Merge Join Plan, it needs to first sort all records  retrieved from both tables and then apply the merge join. So, the cost  of sorting will be additional and hence the overall cost will increase.  So possibly, in this case, the total (including increased) cost is more  than the total cost of Hash Join, so Hash Join is chosen.

最初选择哈希联接是因为未对表中的数据进行排序。为了选择合并联接计划，它需要首先对从两个表中检索到的所有记录进行排序，然后应用合并联接。因此，分类的成本将是额外的，因此总成本将增加。因此，在这种情况下，总的（包括增加的）成本可能大于哈希联接的总成本，因此选择了哈希联接。

Once configuration parameter enable_hashjoin is changed to “off”, this means the query optimizer directly assign a  cost for hash join as disable cost (=1.0e10 i.e. 10000000000.00). The  cost of any possible join will be lesser than this. So, the same query  result in Merge Join after enable_hashjoin changed to “off” as even  including the sorting cost, the total cost of merge join is lesser than  disable cost.

一旦将配置参数enable_hashjoin更改为“ off”，这意味着查询优化器直接将哈希联接的成本分配为禁用成本（= 1.0e10，即10000000000.00）。任何可能的加入的成本都将低于此成本。因此，在enable_hashjoin更改为“ off”后，合并联接中的相同查询结果甚至包括排序成本，合并联接的总成本小于禁用成本。

Now consider the below example:

```sql
postgres=# explain select * from blogtable1, blogtable2 where blogtable1.id1 < blogtable2.id1;
                             QUERY PLAN
--------------------------------------------------------------------------
 Nested Loop  (cost=0.00..150162.50 rows=3333333 width=16)
   Join Filter: (blogtable1.id1 < blogtable2.id1)
   ->  Seq Scan on blogtable1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Materialize  (cost=0.00..20.00 rows=1000 width=8)
      ->  Seq Scan on blogtable2  (cost=0.00..15.00 rows=1000 width=8)
(5 rows)
 
postgres=# set enable_nestloop to off;
SET
postgres=# explain select * from blogtable1, blogtable2 where blogtable1.id1 < blogtable2.id1;
                             QUERY PLAN
--------------------------------------------------------------------------
 Nested Loop  (cost=10000000000.00..10000150162.50 rows=3333333 width=16)
   Join Filter: (blogtable1.id1 < blogtable2.id1)
   ->  Seq Scan on blogtable1  (cost=0.00..145.00 rows=10000 width=8)
   ->  Materialize  (cost=0.00..20.00 rows=1000 width=8)
      ->  Seq Scan on blogtable2  (cost=0.00..15.00 rows=1000 width=8)
(5 rows)
```

As we can see above, even though the nested loop join related  configuration parameter is changed to “off” still it chooses Nested Loop Join as there is no alternate possibility of any other kind of Join  Method to get selected. In simpler terms, since Nested Loop Join is the  only possible join, then whatever is the cost it will be always the  winner (Same as I used to be the winner in 100m race if I ran  alone…:-)). Also, notice the difference in cost in the first and second  plan. The first plan shows the actual cost of Nested Loop Join but the  second one shows the disable cost of the same.

正如我们在上面看到的那样，即使将嵌套循环连接相关的配置参数更改为“ off”，它仍然选择“嵌套循环连接”，因为没有其他选择任何联接方法的可能性。简而言之，由于嵌套循环联接是唯一可能的联接，所以无论付出什么代价，它总是赢家（就像我以前独自参加100m比赛的赢家一样……：-）。另外，请注意第一个和第二个计划的成本差异。第一个计划显示嵌套循环的实际成本加入，但相同的第二个节目禁止成本。

## Conclusion

All kinds of [PostgreSQL](https://severalnines.com/product/clustercontrol/for_postgresql) join methods are useful and get selected based on the nature of the  query, data, join clause, etc. In-case the query is not performing as  expected, i.e. join methods are not selected as expected then, the user  can play around with different plan configuration parameters available  and see if something is missing.

各种[PostgreSQL](https://severalnines.com/product/clustercontrol/for_postgresql)连接方法都是有用的，并可以根据查询，数据，连接子句等的性质进行选择。如果查询未按预期执行，即未按预期选择连接方法，则用户可以尝试使用不同的计划配置参数，看看是否缺少某些内容。



