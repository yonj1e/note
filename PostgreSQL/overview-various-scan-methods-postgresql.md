---
title: PostgreSQL中扫描方法的概述
date: 2019-08-28
categories: 
  - [PostgreSQL]
tags: 
  - JOIN
---

https://severalnines.com/database-blog/overview-various-scan-methods-postgresql

In any of the relational databases engines, it is required to generate a  best possible plan which corresponds to the execution of the query with  least time and resources. Generally, all databases generate plans in a  tree structure format, where the leaf node of each plan tree is called  table scan node. This particular node of the plan corresponds to the  algorithm to be used to fetch data from the base table.

在任何关系数据库引擎中，都需要以最少的时间和资源来生成与查询执行相对应的最佳计划。通常，所有数据库都以树结构格式生成计划，其中每个计划树的叶节点称为表扫描节点。计划的此特定节点对应于用于从基本表中获取数据的算法。

For example, consider a simple query example as SELECT * FROM TBL1, TBL2 where TBL2.ID>1000; and suppose the plan generated is as below:

例如，考虑一个简单的查询示例，例如`SELECT * FROM TBL1, TBL2 where TBL2.ID>1000; `并假设生成的计划如下：

![PostgreSQL Sample Plan Tree](https://severalnines.com/sites/default/files/blog/node_5747/image1.png)

So in the above plan tree, “Sequential Scan on TBL1” and “Index Scan on  TBL2” corresponds to table scan method on table TBL1 and TBL2  respectively. So as per this plan, TBL1 will be fetched sequentially  from the corresponding pages and TBL2 can be accessed using INDEX Scan.

因此，在上述计划树中，“在TBL1上顺序扫描”和“在TBL2上进行索引扫描”分别对应于表TBL1和TBL2上的表扫描方法。因此，按照该计划，将从相应的页面顺序获取TBL1，并且可以使用INDEX Scan访问TBL2。

Choosing the right scan method as part of the plan is very important in terms of overall query performance.

就整体查询性能而言，选择正确的扫描方法作为计划的一部分非常重要。 

Before getting into all types of scan methods supported by PostgreSQL, let’s revise some of the major key points which will be used frequently as we go through the blog.

在介绍PostgreSQL支持的所有类型的扫描方法之前，让我们修改一些重要的要点，这些要点在浏览博客时将经常使用。

![PostgreSQL Data Layout](https://severalnines.com/sites/default/files/blog/node_5747/image2.png)

- **HEAP:** Storage area for storing the whole row of the table. This is divided  into multiple pages (as shown in the above picture) and each page size  is by default 8KB. Within each page, each item pointer (e.g. 1, 2, ….)  points to data within the page.
- **HEAP：**用于存储表的整个行的存储区。它分为多个页面（如上图所示），默认情况下每个页面大小为8KB。在每个页面内，每个项目指针（例如1、2，...）指向页面内的数据。
- **Index Storage:** This storage stores only key values i.e. columns value contained by  index. This is also divided into multiple pages and each page size is by default 8KB.
- **索引存储：**此存储仅存储键值，即索引包含的列值。这也分为多个页面，默认情况下每个页面大小为8KB。
- **Tuple Identifier (TID):** TID is 6 bytes number which consists of two parts. The first part is 4-byte page number and remaining 2 bytes tuple index inside the page. The  combination of these two numbers uniquely points to the storage location for a particular tuple.
- **元组标识符（TID）：** TID是6个字节的数字，由两部分组成。第一部分是4字节的页码，页面内其余2个字节的元组索引。这两个数字的组合唯一地指向特定元组的存储位置。

Currently, PostgreSQL supports below scan methods by which all required data can be read from the table:

当前，PostgreSQL支持以下扫描方法，通过这些方法可以从表中读取所有必需的数据： 

- Sequential Scan
- Index Scan
- Index Only Scan
- Bitmap Scan
- TID Scan

Each of these scan methods are equally useful depending on the query and  other parameters e.g. table cardinality, table selectivity, disk I/O  cost, random I/O cost, sequence I/O cost, etc. Let’s create some  pre-setup table and populate with some data, which will be used  frequently to better explain these scan methods.

根据查询和其他参数（例如，表基数，表选择性，磁盘I/O成本，随机I / O成本，序列I / O成本等），这些扫描方法中的每一种都是同等有用的。让我们创建一些预设置表并填充一些数据，这些数据将经常用于更好地解释这些扫描方法。 

```sql
postgres=# CREATE TABLE demotable (num numeric, id int);
CREATE TABLE
postgres=# CREATE INDEX demoidx ON demotable(num);
CREATE INDEX
postgres=# INSERT INTO demotable SELECT random() * 1000,  generate_series(1, 1000000);
INSERT 0 1000000
postgres=# analyze;
ANALYZE
```

So in this example, one million records are inserted and then the table is analyzed so that all statistics are up to date.

因此，在此示例中，插入了一百万条记录，然后分析该表，以便所有统计信息都是最新的。 

## Sequential Scan

As the name suggests, a Sequential scan of a table is done by sequentially scanning all item pointers of all pages of the corresponding tables. So if there are 100 pages for a particular table and then there are 1000  records in each page, as part of sequential scan it will fetch 100*1000  records and check if it matches as per isolation level and also as per  the predicate clause. So even if only 1 record is selected as part of  the whole table scan, it will have to scan 100K records to find a  qualified record as per the condition.

顾名思义，对表的顺序扫描是通过顺序扫描对应表所有页面的所有项目指针来完成的。因此，如果特定表有100页，然后每页中有1000条记录，那么作为顺序扫描的一部分，它将获取100 * 1000条记录，并根据隔离级别和谓词从句中检查是否匹配。因此，即使在整个表扫描中仅选择1条记录，它也必须扫描100K条记录才能根据条件找到合格的记录。

As per the above table and data, the following query will result in a  sequential scan as the majority of data are getting selected.

根据上面的表和数据，由于选择了大多数数据，因此以下查询将导致顺序扫描。 

```sql
postgres=# explain SELECT * FROM demotable WHERE num < 21000;
                             QUERY PLAN
--------------------------------------------------------------------
 Seq Scan on demotable  (cost=0.00..17989.00 rows=1000000 width=15)
   Filter: (num < '21000'::numeric)
(2 rows)
```

### NOTE

Though without calculating and comparing plan cost, it is almost impossible to tell which kind of scans will be used. But in order for the sequential  scan to be used at-least below criteria should match:

尽管没有计算和比较计划成本，但是几乎无法确定将使用哪种扫描。但是为了至少可以使用顺序扫描，以下条件应匹配：

1. No Index available on key, which is part of the predicate.
2. 键上没有索引可用，这是谓词的一部分。
3. Majority of rows are getting fetched as part of the SQL query.
4. 作为SQL查询的一部分，将获取大多数行。

### TIPS

In case only very few % of rows are getting fetched and the predicate is  on one (or more) column, then try to evaluate performance with or  without index.

如果只有很少的行数被提取并且谓词在一个（或多个）列上，则尝试评估有无索引的性能。 

## Index Scan

Unlike Sequential Scan, Index scan does not fetch all records sequentially.  Rather it uses different data structure (depending on the type of index) corresponding to the index involved in the query and locate required  data (as per predicate) clause with very minimal scans. Then the entry  found using the index scan points directly to data in heap area (as  shown in the above figure), which is then fetched to check visibility as per the isolation level. So there are two steps for index scan:

与顺序扫描不同，索引扫描不会顺序获取所有记录。相反，它使用与查询中涉及的索引相对应的不同数据结构（取决于索引的类型），并以非常少的扫描来查找所需的数据（根据谓词）子句。然后，使用索引扫描找到的条目直接指向堆区域中的数据（如上图所示），然后将其提取以根据隔离级别检查可见性。因此，索引扫描有两个步骤：

- Fetch data from index related data structure. It returns the TID of corresponding data in heap.
- 从与索引相关的数据结构中获取数据。它返回堆中相应数据的TID。
- Then the corresponding heap page is directly accessed to get whole data. This additional step is required for the below reasons:
-  然后直接访问相应的堆页面以获取整个数据。由于以下原因，需要执行此附加步骤 
  - Query might have requested to fetch columns more than whatever available in the corresponding index.
  -  查询可能要求获取列的次数超过了相应索引中的可用列。 
  - Visibility information is not maintained along with index data. So in order to  check the visibility of data as per isolation level, it needs to access  heap data.
  - 可见性信息不会与索引数据一起维护。因此，为了按照隔离级别检查数据的可见性，它需要访问堆数据。

Now we may wonder why not always use Index Scan if it is so efficient. So  as we know everything comes with some cost. Here the cost involved is  related to the type of I/O we are doing. In the case of Index Scan,  Random I/O is involved as for each record found in index storage, it has to fetch corresponding data from HEAP storage whereas in case of  Sequential Scan, Sequence I/O is involved which takes roughly just 25%  of random I/O timing.

现在我们可能想知道为什么如此高效却不总是使用索引扫描。因此，据我们所知，一切都需要付出一定的代价。这里涉及的成本与我们正在执行的I / O类型有关。对于索引扫描，涉及到随机I / O，就像索引存储中找到的每个记录一样，它必须从HEAP存储中获取相应的数据，而对于顺序扫描，则涉及到序列I / O，大约只需要25％ I / O时序的数量。

So Index scan should be chosen only if overall gain outperform the overhead incurred because of Random I/O cost.

 因此，只有在由于随机I / O成本而导致的总增益超过开销的情况下，才应选择索引扫描。 

As per the above table and data, the following query will result in an  index scan as only one record is getting selected. So random I/O is less as well as searching of the corresponding record is quick.

根据上表和数据，由于仅选择一条记录，因此以下查询将导致索引扫描。因此，随机I / O更少，并且相应记录的搜索也很快。

```sql
postgres=# explain SELECT * FROM demotable WHERE num = 21000;
                                QUERY PLAN
--------------------------------------------------------------------------
 Index Scan using demoidx on demotable  (cost=0.42..8.44 rows=1 width=15)
   Index Cond: (num = '21000'::numeric)
(2 rows)
```

## Index Only Scan

Index Only Scan is similar to Index Scan except for the second step i.e. as  the name implies it only scans index data structure. There are two  additional pre-condition in order to choose Index Only Scan compare to  Index Scan:

仅索引扫描与索引扫描类似，不同之处在于第二步，即顾名思义，它仅扫描索引数据结构。为了选择“仅索引扫描”与“索引扫描”，还有两个附加的前提条件：

- Query should be fetching only key columns which are part of the index.
-  查询应仅获取作为索引一部分的键列。 
- All tuples (records) on the selected heap page should be visible. As  discussed in previous section index data structure does not maintain  visibility information so in order to select data only from index we  should avoid checking for visibility and this could happen if all data  of that page are considered visible.
- 所选堆页面上的所有元组（记录）都应该可见。如前一节所述，索引数据结构不保留可见性信息，因此，为了仅从索引中选择数据，我们应避免检查可见性，如果认为该页面上的所有数据都是可见的，则可能会发生这种情况。

The following query will result in an index only scan. Even though this  query is almost similar in terms of selecting number of records but as  only key field (i.e. “num”) is getting selected, it will choose Index  Only Scan.

以下查询将导致仅索引扫描。即使此查询在选择记录数量方面几乎相似，但由于仅选择了关键字段（即“ num”），因此它将选择“仅索引扫描”。

```sql
postgres=# explain SELECT num FROM demotable WHERE num = 21000;
                                  QUERY PLAN
-----------------------------------------------------------------------------
Index Only Scan using demoidx on demotable  (cost=0.42..8.44 rows=1 Width=11)
   Index Cond: (num = '21000'::numeric)
(2 rows)
```

## Bitmap Scan

Bitmap scan is a mix of Index Scan and Sequential Scan. It tries to solve the  disadvantage of Index scan but still keeps its full advantage. As  discussed above for each data found in the index data structure, it  needs to find corresponding data in heap page. So alternatively it needs to fetch index page once and then followed by heap page, which causes a lot of random I/O. So bitmap scan method leverage the benefit of index  scan without random I/O. This works in two levels as below:

位图扫描是索引扫描和顺序扫描的组合。它试图解决索引扫描的缺点，但仍保留其全部优点。如上所述，对于在索引数据结构中找到的每个数据，它需要在堆页面中找到相应的数据。因此，它还需要一次获取索引页，然后再获取堆页，这会导致大量的随机I / O。因此，位图扫描方法利用了索引扫描的优势，而无需随机I / O。这分为两个级别，如下所示：

- Bitmap Index Scan: First it fetches all index data from the index data structure and creates a bit map of all TID. For simple understanding, you can consider this bitmap contains a hash  of all pages (hashed based on page no) and each page entry contains an  array of all offset within that page.
- 位图索引扫描：首先，它从索引数据结构中获取所有索引数据，并创建所有TID的位图。为了简单理解，您可以考虑将此位图包含所有页面的哈希（基于页面编号进行哈希），并且每个页面条目均包含该页面内所有偏移量的数组。
- Bitmap Heap Scan: As the name implies, it reads through bitmap of pages and  then scans data from heap corresponding to stored page and offset. At  the end, it checks for visibility and predicate etc and returns the  tuple based on the outcome of all these checks.
- 位图堆扫描：顾名思义，它会读取页面的位图，然后从堆中扫描与存储的页面和偏移量相对应的数据。最后，它检查可见性和谓词等，并根据所有这些检查的结果返回元组。

Below query will result in Bitmap scan as it is not selecting very few  records (i.e. too much for index scan) and at the same time not  selecting a huge number of records (i.e. too little for a sequential  scan).

下面的查询将导致位图扫描，因为它没有选择非常少的记录（即，对于索引扫描来说太多），而同时没有选择大量的记录（即，对于顺序扫描来说太少了）。 

```sql
postgres=# explain SELECT * FROM demotable WHERE num < 210;
                                  QUERY PLAN
-----------------------------------------------------------------------------
 Bitmap Heap Scan on demotable  (cost=5883.50..14035.53 rows=213042 width=15)
   Recheck Cond: (num < '210'::numeric)
   ->  Bitmap Index Scan on demoidx  (cost=0.00..5830.24 rows=213042 width=0)
      Index Cond: (num < '210'::numeric)
(4 rows)
```

Now consider below query, which selects the same number of records but only key fields (i.e. only index columns). Since it selects only key, it  does not need to refer heap pages for other parts of data and hence  there is no random I/O involved. So this query will choose Index Only  Scan instead of Bitmap Scan.

现在考虑下面的查询，该查询选择相同数量的记录，但仅选择关键字段（即仅索引列）。由于它仅选择密钥，因此不需要为其他部分数据引用堆页面，因此不涉及随机I / O。因此，此查询将选择“仅索引扫描”而不是“位图扫描”。

```sql
postgres=# explain SELECT num FROM demotable WHERE num < 210;
                                   QUERY PLAN
---------------------------------------------------------------------------------------
 Index Only Scan using demoidx on demotable  (cost=0.42..7784.87 rows=208254 width=11)
   Index Cond: (num < '210'::numeric)
(2 rows)
```

## TID Scan

TID, as mentioned above, is 6 bytes number which consists of 4-byte page  number and remaining 2 bytes tuple index inside the page. TID scan is a  very specific kind of scan in PostgreSQL and gets selected only if there is TID in the query predicate. Consider below query demonstrating the  TID Scan:

如上所述，TID是6个字节的数字，其中包括4个字节的页码和页面内剩余的2个字节的元组索引。TID扫描是PostgreSQL中一种非常特殊的扫描，只有在查询谓词中有TID时才被选择。考虑下面显示TID扫描的查询：

```sql
postgres=# select ctid from demotable where id=21000;
   ctid
----------
 (115,42)
(1 row) 
 
postgres=# explain select * from demotable where ctid='(115,42)';
                        QUERY PLAN
----------------------------------------------------------
 Tid Scan on demotable  (cost=0.00..4.01 rows=1 width=15)
   TID Cond: (ctid = '(115,42)'::tid)
(2 rows)
```

So here in the predicate, instead of giving an exact value of the column  as condition, TID is provided. This is something similar to ROWID based  search in Oracle.

因此，在谓词中，没有提供列的确切值作为条件，而是提供了TID。这类似于Oracle中基于ROWID的搜索。

## Bonus

All of the scan methods are widely used and famous. Also, these scan  methods are available in almost all relational database. But there is  another scan method recently in discussion in [the PostgreSQL community](https://severalnines.com/blog/tips-tricks-navigating-postgresql-community) and as well recently added in other relational databases. It is called  “Loose IndexScan” in MySQL, “Index Skip Scan” in Oracle and “Jump Scan”  in DB2.

所有的扫描方法都被广泛使用并出名。同样，这些扫描方法在几乎所有关系数据库中都可用。但是，最近在[PostgreSQL社区](https://severalnines.com/blog/tips-tricks-navigating-postgresql-community)中正在讨论另一种扫描方法，并且最近在其他关系数据库中也添加了另一种扫描方法。它在MySQL中被称为“松散IndexScan”，在Oracle中被称为“索引跳过扫描”，在DB2中被称为“跳转扫描”。

This scan method is used for a specific scenario where in distinct value of  leading key column of B-Tree index is selected. As part of this scan, it avoids traversing all equal key column value rather just traverse the  first unique value and then jump to the next big one. 

此扫描方法用于特定场景，在该场景中选择了B树索引的前导键列的不同值。作为此扫描的一部分，它避免遍历所有相等的键列值，而只是遍历第一个唯一值，然后跳转到下一个大值。 

This work is still in progress in PostgreSQL with the tentative name as “Index Skip Scan” and we may expect to see this in a future release.

这项工作在[PostgreSQL中](https://severalnines.com/product/clustercontrol/for_postgresql)仍在进行中，暂定名称为“索引跳过扫描”，我们可能希望在以后的版本中看到它。

