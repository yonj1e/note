在添加GUC参数时，需要注意你添加的参数属于什么类别的参数。

例如如果你想让普通用户能随时修改它，那么你需要将参数级别设置为PGC_USERSET。如果你想让超级用户能在线修改它，那么你需要将它设置为PGC_SUSET。如果你想让它能够在修改配置参数并通过信号生效，那么需要设置为PGC_SIGHUP。

GUC参数相关的代码如下

```c
src/backend/utils/misc/guc.c
```

## 参数级别介绍

```c
/*  
 * Displayable names for context types (enum GucContext)  
 *  
 * Note: these strings are deliberately not localized.  
 */  
const char *const GucContext_Names[] =  
{  
         /* PGC_INTERNAL */ "internal",					// 编译数据库集群时设置  
         /* PGC_POSTMASTER */ "postmaster",				// 只能启动时设置  
         /* PGC_SIGHUP */ "sighup",						// 允许通过修改配置文件，并通过SIGHUP信号更新参数。  
         /* PGC_SU_BACKEND */ "superuser-backend",		// 超级用户的backend级参数  
         /* PGC_BACKEND */ "backend",					// 普通用户的backend级参数  
         /* PGC_SUSET */ "superuser",					// 允许超级用户在线修改的参数  
         /* PGC_USERSET */ "user"						// 允许普通用户在线修改的参数  
};  
```

如何查看所有参数级别

```sql
postgres=# select context,name,short_desc from pg_settings order by context,category,name;  
      context      |                name                 |                                                          short_desc                                                             
-------------------+-------------------------------------+-------------------------------------------------------------------------------------------------------------------------------  
...
 postmaster        | listen_addresses                    | Sets the host name or IP address(es) to listen to.  
 postmaster        | max_connections                     | Sets the maximum number of concurrent connections.  
 postmaster        | port                                | Sets the TCP port the server listens on.  
 ...
(270 rows)  
```

## 插件中使用示例

以hg_job为例

```c
DefineCustomIntVariable(
	"hgjob.queue_interval",
	gettext_noop("Queue Interval."),
	NULL,
	&JobQueueInterval,
	3,
	1,
	3600,
	PGC_SIGHUP,
	GUC_SUPERUSER_ONLY,
	NULL, NULL, NULL);

DefineCustomStringVariable(
	"hgjob.log_path",
	gettext_noop("Log Path."),
	NULL,
	&JobLogPath,
	logdir,
	PGC_SIGHUP,
	GUC_SUPERUSER_ONLY,
	NULL, NULL, NULL);
```

## 内核中使用示例

以添加闪回参数 `hg_fbq_retention` 为例。

1. 向postgresql.conf.sample模板文件中，添加该参数及说明。

```bash
#hg_fbq_retention = 600			# 600s
```

1. 向guc.c文件添加该参数的定义

```c
#ifdef HGDB_FLASHBACK
	{
                {"hg_fbq_retention", PGC_USERSET, CUSTOM_OPTIONS,
                        gettext_noop("Set flashback time limit."),
                        NULL,
                        GUC_UNIT_S
                },
                &flashbacktimelimit,
                600, 0, INT_MAX,
                NULL, NULL, NULL
        },
#endif
```

1. 在相关头文件中flashback.h定义参数在代码中关联的参数变量

```c
extern int flashbacktimelimit;
```

1. 在相关C文件中flashback.c定义该参数

```c
int		flashbacktimelimit = 600;
```

问题1：向guc添加配置参数时，参数值为bool类型，设置ture或false不能生效。

原因：每个类型都有对应的结构体：

```c
static struct config_bool ConfigureNamesBool[]
static struct config_int ConfigureNamesInt[]
static struct config_real ConfigureNamesReal[]
static struct config_string ConfigureNamesString[]
static struct config_enum ConfigureNamesEnum[]
```

如果参数和结构体类型不对应，可能不会生效。