---

---



## 介绍


本文主要关注PostgreSQL中的索引。

任何主题都可以从不同的角度考虑。我们将讨论应用程序开发人员使用DBMS感兴趣的点：可用的索引类型，为什么有这么多不同类型的索引，以及如何使用它们来加快查询。

这个主题也许可以用更少的篇幅来描述，但是我们希望作为一个好奇的开发人员，也可以对内部细节感兴趣，特别是了解这些细节不仅可以让您遵从他人的判断，还可以得出自己的结论。

新类型索引的开发超出了讨论范围。这需要了解C语言编程，并且属于系统程序员而不是应用程序开发人员的专业知识。出于同样的原因，我们不讨论编程接口，只关注使用索引相关的内容。

接下来，我们将讨论与DBMS核心相关的**通用索引引擎**和各个索引访问方法之间的职责分配，PostgreSQL允许我们将这些方法添加为扩展。在下一篇文章中，我们将讨论[访问方法](https://habr.com/ru/company/postgrespro/blog/442546/)的[接口](https://habr.com/ru/company/postgrespro/blog/442546/)和关键概念，如类和运算符。之后，将考虑不同类型索引的结构和应用的细节：[Hash](https://habr.com/ru/company/postgrespro/blog/442776/)，[B-tree](https://habr.com/ru/company/postgrespro/blog/443284/)，[GiST](https://habr.com/ru/company/postgrespro/blog/444742/)，[SP-GiST](https://habr.com/ru/company/postgrespro/blog/446624/)，[GIN](https://habr.com/ru/company/postgrespro/blog/448746/)和[RUM](https://habr.com/ru/company/postgrespro/blog/452116/)，[BRIN](https://habr.com/ru/company/postgrespro/blog/452900/)和[Bloom](https://habr.com/ru/company/postgrespro/blog/452968/)。

## 索引

在PostgreSQL中，索引是一种特殊的数据库对象，主要用于加速数据访问。索引是辅助结构：可以从表中的信息删除和重新创建每个索引。索引还可用于强制执行某些完整性约束。

目前，PostgreSQL 9.6内置了六种不同类型的索引，并且由于版本9.6的重大更改，还提供了一个索引作为扩展。因此，预计不久的将来会出现新的索引类型。

尽管索引类型（也称为访问方法）之间存在差异，但它们最终都将一个键(例如，索引列的值)与包含该键的表的行关联起来。每一行由TID（元组id）标识，它由文件中的块数和块内行的位置组成。也就是说，使用已知键或有关它的一些信息，我们可以快速读取那些可能包含我们感兴趣的信息的行，而无需扫描整个表。

重要的是要理解索引以一定的维护成本加速数据访问。对于索引数据上的每个操作，无论是表行的插入，删除还是更新，都需要在同一个事务中更新该表的索引。请注意，更新尚未构建索引的表字段不会导致索引更新; 这种技术叫做HOT（Heap-Only Tuples）。

可扩展性带来一些影响。为了方便向系统添加新的访问方法，实现了通用索引引擎接口。它的主要任务是从访问方法中获取TID并使用它们：

- 从相应版本的表行中读取数据。
- 通过TID获取行版本TID，或使用预构建的位图批量获取行版本。
- 检查当前事务的行版本的可见性，同时考虑其隔离级别。

索引引擎参与执行查询。它是根据优化阶段创建的计划调用的。优化器对执行查询的不同方法进行排序和评估，应该了解所有可能适用的访问方法功能。该方法是否能够按所需的顺序返回数据，或者我们是否应该预期排序？我们可以使用此方法搜索NULL吗？这些是优化器经常要解决的问题。

不仅优化器需要有关访问方法的信息。在构建索引时，系统必须决定是否可以在多个列上构建索引，以及此索引是否确保唯一性。

因此，每种访问方法都应提供有关自身的所有必要信息。低于9.6的版本使用了pg_am表，而从版本9.6开始，数据移动到更深层的特殊函数中。我们将进一步了解这个接口。

剩下的就是访问方法的任务：

- 实现用于构建索引的算法，并将数据映射到页面中（以便缓冲区缓存管理器统一处理每个索引）。
-  通过“索引字段操作符表达式”形式的谓词在索引中搜索信息。
- 评估索引使用成本。
- 操纵正确并行处理所需的锁。
- 生成预写日志（WAL）记录。

我们将首先考虑通用索引引擎的功能，然后再考虑不同的访问方法。

## 索引引擎

索引引擎使PostgreSQL能够统一地处理各种访问方法，但要考虑它们的特性。

### 主要扫描技术

#### 索引扫描


我们可以用不同的方法处理由索引提供的TID。我们来看一个例子：

```sql
postgres=# create table t(a integer, b text, c boolean);
postgres=# insert into t(a,b,c)
  select s.id, chr((32+random()*94)::integer), random() < 0.01
  from generate_series(1,100000) as s(id)
  order by random();
postgres=# create index on t(a);
postgres=# analyze t;
```

我们创建了一个包含三个字段的表。第一个字段包含1到100.000之间的数字，并且在此字段上创建索引（无论何种类型）。第二个字段包含除不可打印字符之外的各种ASCII字符。最后，第三个字段包含一个逻辑值，约1％的行是true，其余的是false。行以随机顺序插入表中。

让我们尝试通过条件a = 1来选择一个值。请注意，条件看起来像“ *索引字段操作符表达式* ”，其中*操作**符*为“相等”，*表达式*（搜索键）为“1”。在大多数情况下，要使用的索引，条件必须如此。

```sql
postgres=# explain (costs off) select * from t where a = 1;
          QUERY PLAN          
-------------------------------
 Index Scan using t_a_idx on t
   Index Cond: (a = 1)
(2 rows)
```

在这种情况下，优化器决定使用*索引扫描*。通过索引扫描，访问方法逐个返回TID值，直到达到最后一个匹配的行。索引引擎依次访问由TID指示的表行，获取行版本，检查其对多版本并发规则的可见性，并返回获得的数据。

#### 位图扫描

当我们只处理几个值时，索引扫描工作正常。但是，随着检索到的行数增加，很有可能多次返回同一个表页。因此，优化器切换*到位图扫描*。

```sql
postgres=# explain (costs off) select * from t where a <= 100;
             QUERY PLAN            
------------------------------------
 Bitmap Heap Scan on t
   Recheck Cond: (a <= 100)
   ->  Bitmap Index Scan on t_a_idx
         Index Cond: (a <= 100)
(4 rows)
```

访问方法首先返回所有与条件匹配的TID（位图索引扫描节点），并从这些TID构建行版本的位图。然后从表中读取行版本（位图堆扫描），每个页面只读取一次。

请注意，在第二步中，可能会重新检查条件（Recheck Cond）。对于行版本的位图来说，检索的行数可能太大，无法完全匹配RAM(受work_mem参数的限制)。在这种情况下，仅为包含至少一个匹配行版本的页面构建位图。

这个“有损”位图需要的空间更少，但在阅读页面时，我们需要重新检查其中包含的每一行的条件。请注意，即使对于少量检索到的行并因此“精确”位图（例如在我们的示例中），«Recheck Cond»步骤仍然在计划中表示，尽管实际上并未执行。

如果对多个表字段施加条件并对这些字段建立索引，位图扫描允许同时使用多个索引（如果优化程序认为这样有效）。对于每个索引，构建行版本的位图，然后执行按位布尔乘法（如果表达式由AND连接）或布尔加法（如果表达式由OR连接）。例如：

```sql
postgres=# create index on t(b);
postgres=# analyze t;
postgres=# explain (costs off) select * from t where a <= 100 and b = 'a';
                    QUERY PLAN                    
--------------------------------------------------
 Bitmap Heap Scan on t
   Recheck Cond: ((a <= 100) AND (b = 'a'::text))
   ->  BitmapAnd
         ->  Bitmap Index Scan on t_a_idx
               Index Cond: (a <= 100)
         ->  Bitmap Index Scan on t_b_idx
               Index Cond: (b = 'a'::text)
(7 rows)
```

这里BitmapAnd节点通过按位与操作连接两个位图。

位图扫描使我们能够避免重复访问相同的数据页。但是如果表页面中数据的物理排序方式与索引记录完全相同怎么办？毫无疑问，我们不能完全依赖页面中数据的物理顺序。如果需要排序数据，我们必须在查询中显式指定ORDER BY子句。但实际情况可能是“几乎所有”数据都被排序的情况：例如，如果按所需顺序添加行，并且在此之后或执行CLUSTER命令之后不进行更改。在这种情况下，构建位图是一个过度的步骤，常规索引扫描也同样好（除非我们考虑连接多个索引的可能性）。因此，在选择访问方法时，计划器会查看一个特殊的统计数据，该统计数据显示了物理行排序和列值逻辑排序之间的相关性:

```sql
postgres=# select attname, correlation from pg_stats where tablename = 't';
 attname | correlation
---------+-------------
 b       |    0.533512
 c       |    0.942365
 a       | -0.00768816
(3 rows)
```

接近1的绝对值表示高相关性（对于列c），而接近于0的值则相反，表示混沌分布（列a）。

#### 顺序扫描

在非选择性条件下，优化器将优先选择整个表的顺序扫描而不是使用索引：

```sql
postgres=# explain (costs off) select * from t where a <= 40000;
       QUERY PLAN      
------------------------
 Seq Scan on t
   Filter: (a <= 40000)
(2 rows)
```

问题是条件选择性越高，索引就越好，也就是说，匹配它的行越少。检索行数的增长增加了读取索引页的开销成本。

顺序扫描比随机扫描更快，这使情况更加复杂。这特别适用于硬盘，在硬盘上，将磁头带到轨道的机械操作比读取数据本身花费更多的时间。SSD的效果不太明显。有两个参数可用于考虑访问成本的差异，seq_page_cost和random_page_cost，我们不仅可以在全局设置，还可以在表空间级别设置，这种方法可以调整不同磁盘子系统的特性。

### 仅索引扫描

通常，访问方法的主要任务是返回匹配表行的标识符，以便索引引擎从这些行读取必要的数据。但是，如果索引已包含查询所需的所有数据呢？这样的索引称为*覆盖*，在这种情况下，优化器可以应用*仅索引扫描*：

```sql
postgres=# vacuum t;
postgres=# explain (costs off) select a from t where a < 100;
             QUERY PLAN            
------------------------------------
 Index Only Scan using t_a_idx on t
   Index Cond: (a < 100)
(2 rows)
```

此名称可能让人觉得索引引擎根本不访问该表，而是仅从访问方法获取所有必要信息。但事实并非如此，因为PostgreSQL中的索引并不存储使我们能够判断行可见性的信息。因此，访问方法返回与搜索条件匹配的行版本，而不考虑它们在当前事务中的可见性。

但是，如果索引引擎每次都需要查看表以获得可见性，那么这种扫描方法与常规索引扫描没有什么不同。

为解决这个问题，PostgreSQL为表维护了一个所谓的*可见性映射，*在这个映射中，不管起始时间和隔离级别如何，标记清理的页面中，如果数据更改的时间不够长，数据可被所有事务看到。如果索引返回的行的标识符与这样的页面相关，则可以避免可见性检查。

因此，定期vacuum可以提高覆盖指数的效率。此外，优化器会考虑死元组的数量，如果预测可见性检查的开销很高，则可以决定不使用仅索引扫描。

我们可以使用EXPLAIN ANALYZE命令了解对表的强制访问次数：

```sql
postgres=# explain (analyze, costs off) select a from t where a < 100;
                                  QUERY PLAN                                  
-------------------------------------------------------------------------------
 Index Only Scan using t_a_idx on t (actual time=0.025..0.036 rows=99 loops=1)
   Index Cond: (a < 100)
   Heap Fetches: 0
 Planning time: 0.092 ms
 Execution time: 0.059 ms
(5 rows)
```

在这种情况下，不需要访问表（Heap Fetches：0），因为刚刚进行了vacuum操作。一般来说，这个数字越接近零越好。

并非所有索引都与行标识符一起存储索引值。如果访问方法无法返回数据，则不能将其用于仅索引扫描。

PostgreSQL 11引入了一项新特性：INCLUDE-indexes。如果有一个唯一索引缺少某些列可用作某些查询的覆盖索引，该怎么办？不能简单地将列添加到索引中，因为它会破坏其唯一性。该特性允许包含不影响唯一性且不能用于搜索谓词中的*非键*列，但仍可以提供仅索引扫描。该补丁由我的同事Anastasia Lubennikova开发。

上文中描述了索引、索引类型及主要的扫描技术，在接下来的内容中继续介绍与索引相关的内容，主要包括空值与索引、索引类型示例、索引与排序、并行创建。

### 空值

NULL作为表示不存在或未知值的一种方便的方法，在关系数据库中发挥着重要的作用。

但是特殊的值需要特殊的处理。常规布尔代数变为三元代数; 目前还不清楚NULL是小于还是大于常规值（这需要特殊的排序结构，NULLS FIRST和NULLS LAST）; 聚合函数是否应该考虑NULL是不明显的; 规划者需要一个特殊的统计数据...... 

从索引支持的角度来看，我们还不清楚是否需要对这些值进行索引。如果未对NULL进行索引，则索引可能更紧凑。但是如果对NULL进行索引，我们能够将索引用于像“ *indexed-field* IS [NOT] NULL“这样的条件，并且当没有为表指定条件时也可以作为覆盖索引（因为在这种情况下，索引必须返回所有表的数据行，包括那些使用NULL值的行）。

对于每种访问方法，开发人员决定是否对NULL进行索引。但通常情况下，它们会被编入索引。

### 多列索引

要支持多个字段的条件，可以使用*多列索引*。例如，我们可以在表的两个字段上构建索引：

```sql
postgres=# create index on t(a,b);
postgres=# analyze t;
```

优化器很可能更喜欢这个索引，而不是加入位图，因为在这里我们很容易获得所需的TID而无需任何辅助操作：

```sql
postgres=# explain (costs off) select * from t where a <= 100 and b = 'a';
                   QUERY PLAN                  
------------------------------------------------
 Index Scan using t_a_b_idx on t
   Index Cond: ((a <= 100) AND (b = 'a'::text))
(2 rows)
```

多列索引还可以通过某些字段的条件(从第一个字段开始)加快数据检索速度：

```sql
postgres=# explain (costs off) select * from t where a <= 100;
              QUERY PLAN              
--------------------------------------
 Bitmap Heap Scan on t
   Recheck Cond: (a <= 100)
   ->  Bitmap Index Scan on t_a_b_idx
         Index Cond: (a <= 100)
(4 rows)
```

通常，如果第一个字段没有附加条件，则不使用索引。但有时优化器可能会认为索引的使用比顺序扫描更有效。在考虑btree索引时，我们将扩展这个主题。

并非所有访问方法都支持在多个列上构建索引。

### 表达式索引

我们已经提到搜索条件必须看起来像“ *索引字段操作符表达式* ”。在下面的示例中，将不使用索引，因为使用包含字段名称的表达式而不是字段名称本身：

```sql
postgres=# explain (costs off) select * from t where lower(b) = 'a';
                QUERY PLAN                
------------------------------------------
 Seq Scan on t
   Filter: (lower((b)::text) = 'a'::text)
(2 rows)
```

重写此特定查询并不需要太多时间，只要字段名称写入操作符的左侧。如果这不允许，表达式索引(函数索引)将会有所帮助：

```sql
postgres=# create index on t(lower(b));
postgres=# analyze t;
postgres=# explain (costs off) select * from t where lower(b) = 'a';
                     QUERY PLAN                    
----------------------------------------------------
 Bitmap Heap Scan on t
   Recheck Cond: (lower((b)::text) = 'a'::text)
   ->  Bitmap Index Scan on t_lower_idx
         Index Cond: (lower((b)::text) = 'a'::text)
(4 rows)
```

函数索引不是建立在表字段上，而是建立在任意表达式上。优化器会将此索引视为“*索引表达式操作符表达式* ”之类的条件。如果要索引表达式的计算代价高昂，那么索引的更新也将需要大量的计算资源。

还请记住，为索引表达式收集了单独的统计信息。可以通过索引名称在pg_stats视图中了解这个统计信息：

```sql
postgres=# \d t
       Table "public.t"
 Column |  Type   | Modifiers
--------+---------+-----------
 a      | integer |
 b      | text    |
 c      | boolean |
Indexes:
    "t_a_b_idx" btree (a, b)
    "t_a_idx" btree (a)
    "t_b_idx" btree (b)
    "t_lower_idx" btree (lower(b))
postgres=# select * from pg_stats where tablename = 't_lower_idx';
```

如果有必要，可以使用与常规数据字段相同的方式控制直方图的栏的数量（注意，根据索引表达式，列名可能不同）：

```sql
postgres=# \d t_lower_idx
 Index "public.t_lower_idx"
 Column | Type | Definition
--------+------+------------
 lower  | text | lower(b)
btree, for table "public.t"
postgres=# alter index t_lower_idx alter column "lower" set statistics 69;
```

PostgreSQL 11引入了一种更简洁的方法，通过在ALTER INDEX ... SET STATISTICS命令中指定列*号*来控制索引的统计目标。补丁由我的同事Alexander Korotkov和Adrien Nayrat开发。

### 部分索引

有时需要仅索引表行的一部分。这通常与高度不均匀的分布有关：通过索引搜索不频繁的值是有意义的，但通过对表的完全扫描更容易找到频繁值。

我们当然可以在«c»列上建立一个常规索引，它将以我们期望的方式工作：

```sql
postgres=# create index on t(c);
postgres=# analyze t;
postgres=# explain (costs off) select * from t where c;
          QUERY PLAN          
-------------------------------
 Index Scan using t_c_idx on t
   Index Cond: (c = true)
   Filter: c
(3 rows)
postgres=# explain (costs off) select * from t where not c;
    QUERY PLAN    
-------------------
 Seq Scan on t
   Filter: (NOT c)
(2 rows)
```

索引大小为276页：

```sql
postgres=# select relpages from pg_class where relname='t_c_idx';
 relpages
----------
      276
(1 row)
```

但由于«c»列仅对1％的行具有true值，因此99％的索引实际上从未使用过。在这种情况下，我们可以构建一个部分索引：

```sql
postgres=# create index on t(c) where c;
postgres=# analyze t;
```

索引的大小减少到5页：

```sql
postgres=# select relpages from pg_class where relname='t_c_idx1';
 relpages
----------
        5
(1 row)
```

有时，大小和性能上的差异可能非常显著。

### 排序

如果访问方法以某种特定顺序返回行标识符，这将为优化器提供额外的选项来执行查询。

我们可以扫描表格，然后对数据进行排序：

```sql
postgres=# set enable_indexscan=off;
postgres=# explain (costs off) select * from t order by a;
     QUERY PLAN      
---------------------
 Sort
   Sort Key: a
   ->  Seq Scan on t
(3 rows)
```

但是我们可以很容易地按照所需的顺序使用索引读取数据：

```sql
postgres=# set enable_indexscan=on;
postgres=# explain (costs off) select * from t order by a;
          QUERY PLAN          
-------------------------------
 Index Scan using t_a_idx on t
(1 row)
```

所有访问方法中只有«btree»可以返回排序数据，所以让我们推迟更详细的讨论，直到考虑到这种类型的索引。

### 并发构建

通常构建索引会获取表的SHARE锁。此锁允许从表中读取数据，但禁止在构建索引时进行任何更改。

我们可以确保这一点，例如，在表«t»中构建索引时，我们在另一个会话中执行以下查询：

```sql
postgres=# select mode, granted from pg_locks where relation = 't'::regclass;
   mode    | granted
-----------+---------
 ShareLock | t
(1 row)
```


如果表足够大并且广泛用于插入，更新或删除，这可能是不允许的，因为修改进程将等待锁释放很长时间。

在这种情况下，我们可以使用并发构建索引。

```sql
postgres=# create index concurrently on t(a);
```

此命令以SHARE UPDATE EXCLUSIVE模式锁定表，该模式允许读取和更新（仅禁止更改表结构，以及并发清理，分析或在此表上构建另一个索引）。

然而，也有不利的一面。首先，索引将比平常构建得更慢，因为在表中完成两次传递而不是一次，并且还需要等待完成修改数据的并行事务。

其次，在并发构建索引时，可能会发生死锁或者违反唯一约束。但是，该索引将被建立，尽管尚未运行。必须删除并重建此类索引。非操作索引在psql \ d命令的输出中用INVALID字标记，下面的查询返回这些索引的完整列表：

```sql
postgres=# select indexrelid::regclass index_name, indrelid::regclass table_namefrom pg_index where not indisvalid;
 index_name | table_name
------------+------------
 t_a_idx    | t
(1 row)
```

## 接口

在[第一篇文章中](https://habr.com/ru/company/postgrespro/blog/441962/)，我们已经提到访问方法（即索引类型）必须提供自身的相关信息。让我们看一下访问方法接口的结构。

### 属性

访问方法的所有属性都存储在«pg_am»表中（“am”代表访问方法）。我们还可以从同一个表中获取可用方法的列表：

```sql
postgres=# select amname from pg_am;
 amname
--------
 btree
 hash
 gist
 gin
 spgist
 brin
(6 rows)
```

虽然顺序扫描可以正确地引用访问方法，但由于历史原因，它不在此列表中。

在PostgreSQL 9.5及更低版本中，每个属性都用«pg_am»表的单独字段表示。从9.6版本开始，属性使用特殊函数进行查询，并被分为多个层：

- 访问方法属性 - «pg_indexam_has_property»
- 特定索引的属性 - «pg_index_has_property»
- 索引各列的属性 - «pg_index_column_has_property»

展望未来，访问方法层和索引层是分开的：到目前为止，所有基于一种访问方法的索引始终具有相同的属性。

**以下是访问方法的四个属性**（通过«btree»的示例）：

```sql
postgres=# select a.amname, p.name, pg_indexam_has_property(a.oid,p.name)from pg_am a,
     unnest(array['can_order','can_unique','can_multi_col','can_exclude']) p(name)where a.amname = 'btree'order by a.amname;
 amname |     name      | pg_indexam_has_property
--------+---------------+-------------------------
 btree  | can_order     | t
 btree  | can_unique    | t
 btree  | can_multi_col | t
 btree  | can_exclude   | t
(4 rows)
```

- can_order

  访问方法使我们能够在创建索引时指定值的排序顺序（目前仅适用于«btree»）。

- can_unique

  支持唯一约束和主键（仅适用于«btree»）。

- can_multi_col

  可以在多个列上构建索引。

- can_exclude

  支持排除约束EXCLUDE。


**以下属性与索引有关**（让我们以一个现有的索引为例）：

```sql
postgres=# select p.name, pg_index_has_property('t_a_idx'::regclass,p.name)from unnest(array[
       'clusterable','index_scan','bitmap_scan','backward_scan'
     ]) p(name);
     name      | pg_index_has_property
---------------+-----------------------
 clusterable   | t
 index_scan    | t
 bitmap_scan   | t
 backward_scan | t
(4 rows)
```

-  clusterable

  可以根据索引重新排序行（使用同名命令CLUSTER进行）。

- index_scan

  支持索引扫描。尽管此属性可能看起来很奇怪，但并非所有索引都可以逐个返回TID - 有些索引一次返回所有结果，并且只支持位图扫描。

- bitmap_scan

  支持位图扫描。

- backward_scan

  可以按照构建索引时指定的顺序返回结果。

**以下是列属性：**

```sql
postgres=# select p.name,
     pg_index_column_has_property('t_a_idx'::regclass,1,p.name)from unnest(array[
       'asc','desc','nulls_first','nulls_last','orderable','distance_orderable',
       'returnable','search_array','search_nulls'
     ]) p(name);
        name        | pg_index_column_has_property
--------------------+------------------------------
 asc                | t
 desc               | f
 nulls_first        | f
 nulls_last         | t
 orderable          | t
 distance_orderable | f
 returnable         | t
 search_array       | t
 search_nulls       | t
(9 rows)
```

- asc，desc，nulls_first，nulls_last，orderable

  这些属性与值的排序有关（我们将在讨论«btree»索引时进一步描述）。

- distance_orderable

  结果可以按操作确定的排序顺序返回（目前仅适用于GiST和RUM索引）。

-  returnable

  可以在不访问表的情况下使用索引，即支持仅索引扫描。

- search_array

  支持使用表达式« *indexed-field* IN（*list_of_constants*）» 搜索多个值，该表达式与« *indexed-field* = ANY（*array_of_constants*）»相同。

- search_nulls

  可以通过IS NULL和IS NOT NULL条件进行搜索。


我们已经详细讨论了一些属性。有些属性是特定于某些访问方法的。我们将在考虑这些具体方法时进一步展开描述。

### 运算符类和族

除了由所描述的接口公开的访问方法的属性之外，还需要信息来了解访问方法接受哪些数据类型和哪些操作符。为此，PostgreSQL引入了*运算符类*和*运算符族*概念。

运算符类包含用于操作特定数据类型的索引的最小运算符集（可能还有辅助函数）。

运算符类包含在某些运算符族中。此外，如果一个公共运算符族具有相同的语义，则它们可以包含多个运算符类。例如，«integer_ops»系列包括«int8_ops»，«int4_ops»和«int2_ops»类，用于类型«bigint»，«integer»和«smallint»，它们的大小不同，但含义相同：

```sql
postgres=# select opfname, opcname, opcintype::regtypefrom pg_opclass opc, pg_opfamily opfwhere opf.opfname = 'integer_ops'and opc.opcfamily = opf.oidand opf.opfmethod = (
      select oid from pg_am where amname = 'btree'
    );
   opfname   | opcname  | opcintype
-------------+----------+-----------
 integer_ops | int2_ops | smallint
 integer_ops | int4_ops | integer
 integer_ops | int8_ops | bigint
(3 rows)
```


另一个例子：«datetime_ops»系列包括操作日期的运算符类（有时间和无时间）：

```sql
postgres=# select opfname, opcname, opcintype::regtypefrom pg_opclass opc, pg_opfamily opfwhere opf.opfname = 'datetime_ops'and opc.opcfamily = opf.oidand opf.opfmethod = (
      select oid from pg_am where amname = 'btree'
    );
   opfname    |     opcname     |          opcintype          
--------------+-----------------+-----------------------------
 datetime_ops | date_ops        | date
 datetime_ops | timestamptz_ops | timestamp with time zone
 datetime_ops | timestamp_ops   | timestamp without time zone
(3 rows)
```


运算符族还可以包括其他运算符来比较不同类型的值。将谓词分组为多个族，使计划器能够为具有不同类型值的谓词使用索引。一个族也可以包含其他辅助功能。

在大多数情况下，我们不需要了解有关运算符族和类的任何信息。通常我们只是创建一个索引，默认情况下使用某个运算符类。

但是，我们可以显式指定运算符类。这是一个简单的示例，说明何时需要显式规范：在排序规则与C不同的数据库中，常规索引不支持LIKE操作：

```sql
postgres=# show lc_collate;
 lc_collate 
-------------
 en_US.UTF-8
(1 row)
```

```sql
postgres=# explain (costs off) select * from t where b like 'A%';
         QUERY PLAN          
-----------------------------
 Seq Scan on t
   Filter: (b ~~ 'A%'::text)
(2 rows)
```


我们可以通过使用运算符类«text_pattern_ops»创建索引来克服此限制（注意计划中的条件如何更改）：

```sql
postgres=# create index on t(b text_pattern_ops);
postgres=# explain (costs off) select * from t where b like 'A%';
                           QUERY PLAN                          
----------------------------------------------------------------
 Bitmap Heap Scan on t
   Filter: (b ~~ 'A%'::text)
   ->  Bitmap Index Scan on t_b_idx1
         Index Cond: ((b ~>=~ 'A'::text) AND (b ~<~ 'B'::text))
(4 rows)
```

### 系统表


在本文的结尾，我们提供了系统目录中与运算符类和族直接相关的表的简化图。

 ![IMG_256](indexs-in-postgresql/clip_image002.png)

不用多说，所有这些表都有详细的描述。

系统目录使我们无需查找文档即可找到许多问题的答案。例如，某种访问方法可以操纵哪些数据类型？

```sql
postgres=# select opcname, opcintype::regtypefrom pg_opclasswhere opcmethod = (select oid from pg_am where amname = 'btree')order by opcintype::regtype::text;
       opcname       |          opcintype          
---------------------+-----------------------------
 abstime_ops         | abstime
 array_ops           | anyarray
 enum_ops            | anyenum
...
```


运算符类包含哪些运算符（因此，索引访问可用于包含此类运算符的条件）？

```sql
postgres=# select amop.amopopr::regoperatorfrom pg_opclass opc, pg_opfamily opf, pg_am am, pg_amop amopwhere opc.opcname = 'array_ops'and opf.oid = opc.opcfamilyand am.oid = opf.opfmethodand amop.amopfamily = opc.opcfamilyand am.amname = 'btree'and amop.amoplefttype = opc.opcintype;
        amopopr        
-----------------------
 <(anyarray,anyarray)
 <=(anyarray,anyarray)
 =(anyarray,anyarray)
 >=(anyarray,anyarray)
 >(anyarray,anyarray)
(5 rows)
```

## HASH

### 结构

#### 普遍理论

现代许多编程语言都将哈希表作为基本的数据类型。在外部，哈希表看起来像一个常规数组，它使用任何数据类型(例如字符串)而不是整数作为索引。PostgreSQL中的哈希索引也是以类似的方式构造的。那么它是如何运作的？

通常，数据类型的允许值范围非常大：在“text”类型的列中我们可以设想多少个不同的字符串？同时，在某个表的文本列中实际存储了多少个不同的值？通常情况下，没有那么多。

哈希的思想是将少量的数字（从0到*N* -1，总共*N个*值）与任何数据类型的值相关联。这样的关联称为*哈希函数*。所获得的数字可用作常规数组的索引，其中将存储对表行（TID）的引用。该数组的元素称为*哈希表存储桶（bucket）*-如果相同的索引值出现在不同的行中，则一个存储桶可以存储多个TID。

哈希函数按存储桶分配源值的方式越统一，效果越好。但是，即使是一个良好的哈希函数，有时也会对不同的源值产生相等的结果——这称为*冲突*。一个存储桶可以存储对应于不同键的TID，但因此需要重新检查从索引获得的TID。

举个例子：我们可以想到字符串的哪些哈希函数？假设存储桶的数量为256。然后，以桶的编号为例，我们可以采用第一个字符的代码（假设单字节字符编码）。这是一个很好的哈希函数吗？显然不是：如果所有字符串都以相同的字符开头，则所有字符串都将放入一个存储桶中，因此毫无疑问是统一的，所有值都需要重新检查，哈希将毫无意义。如果我们将所有以256为模的字符的代码相加怎么办？这样会更好，但远非理想。如果您对PostgreSQL中这种哈希函数的内部结构感兴趣，请查看[hashfunc.c](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/access/hash/hashfunc.c;hb=HEAD)中hash_any（）的定义。

#### 索引结构


让我们回到哈希索引。对于某些数据类型（索引键）的值，我们的任务是快速找到匹配的TID。

当插入索引时，让我们计算键的哈希函数。PostgreSQL中的哈希函数总是返回«integer»类型，其范围为2的32次方≈40亿个值。存储桶的数量最初等于2，然后根据数据大小动态增加。存储桶编号可以使用位算法从哈希码中计算出来。这是我们将放置TID的存储桶。

但这还不够，因为可以将与不同键匹配的TID放入同一存储桶中。我们该怎么办？除了TID之外，还可以将键的源值存储在存储桶中，但这会大大增加索引大小。为了节省空间，存储桶不存储键，而是存储了键的哈希代码。

当搜索索引时，我们计算键的哈希函数并获取存储桶编号。现在，仍然需要遍历存储桶的内容，并仅返回具有适当哈希码的匹配TID。由于存储的“hash code - TID”对是有序的，因此可以高效地完成此操作。

但是，两个不同的键不仅可能会进入一个存储桶，而且可能会具有相同的四字节哈希码——没有人能消除冲突。因此，访问方法要求通用索引引擎通过重新检查表行中的条件来验证每个TID（引擎可以在进行可见性检查的同时执行此操作）。

#### 将数据结构映射到页面


如果我们从缓冲区缓存管理器的角度而不是从查询计划和执行的角度看待索引，那么所有信息和所有索引行都必须打包到页面中。这样的索引页存储在缓存中，并以与表页完全相同的方式从缓存中删除。

 ![IMG_256](indexs-in-postgresql/clip_image002.gif)

如图所示，哈希索引使用四种页面（灰色矩形）：

- 元页面-第0页，其中包含有关索引内部内容的信息。

- 存储桶页面-索引的主页，将数据存储为“哈希码-TID”对。

- 溢出页面-与存储桶页面的结构相同，当一个存储桶超出一个页面时使用。

- 位图页面-跟踪当前清除的溢出页面，这些页面可重新用于其他存储桶。



从索引页元素开始的向下箭头表示TID，即对表行的引用。

每次索引增加，PostgreSQL即时创建的存储桶（也就是页面）数量是上次创建的存储桶数量的两倍。为了避免一次分配大量潜在的页面，版本10使大小增加更加平稳。对于溢出页面，它们会根据需要进行分配，并在位图页面中进行跟踪，位图页面也会根据需要进行分配。

请注意，哈希索引不能减​​小大小。如果我们删除一些索引行，则已经分配的页面不会返回给操作系统，而只会在VACUUMING之后重新用于新数据。减小索引大小的唯一选项是使用REINDEX或VACUUM FULL命令从头开始重建索引。

### 例子


让我们看看如何创建哈希索引。为了避免设计自己的表，从现在开始，我们将使用航空运输[的演示数据库](https://postgrespro.com/docs/postgrespro/9.6/demodb-bookings)，这次我们将使用flights表。

```sql
demo=# create index on flights using hash(flight_no);
WARNING:  hash indexes are not WAL-logged and their use is discouraged
CREATE INDEX
```

```sql
demo=# explain (costs off) select * from flights where flight_no = 'PG0001';
                     QUERY PLAN                     
----------------------------------------------------
 Bitmap Heap Scan on flights
   Recheck Cond: (flight_no = 'PG0001'::bpchar)
   ->  Bitmap Index Scan on flights_flight_no_idx
         Index Cond: (flight_no = 'PG0001'::bpchar)
(4 rows)
```


当前哈希索引实现的不便之处在于，带有索引的操作未记录在预写日志中（PostgreSQL在创建索引时会发出警告）。因此，哈希索引在失败后将无法恢复，并且不参与复制。此外，哈希索引的通用性远低于“ B-tree”，其效率也值得怀疑。因此，现在使用这样的索引是不切实际的。

但是，在PostgreSQL的10版本发布后，这种情况最早将在今年秋天（2017年）改变。在此版本中，哈希索引最终获得了对预写日志的支持；此外，还优化了内存分配，并发工作更加高效。

确实如此。由于PostgreSQL 10哈希索引得到了全面的支持，因此可以不受限制地使用。警告将不再显示。

### 哈希语义


但是，为什么无法使用的哈希索引几乎从PostgreSQL诞生之日起到9.6就一直存在？问题是DBMS广泛使用了哈希算法（特别是用于哈希连接和分组），并且系统必须知道将哪种哈希函数应用于哪种数据类型。但是这种对应关系不是一成不变的，并且不能一劳永逸地设置，因为PostgreSQL允许动态添加新的数据类型。这是一种通过哈希的访问方法，其中存储了此对应关系，表示为辅助函数与运算符族的关联。

```sql
demo=# select   opf.opfname as opfamily_name,
         amproc.amproc::regproc AS opfamily_procedurefrom     pg_am am,
         pg_opfamily opf,
         pg_amproc amprocwhere    opf.opfmethod = am.oidand      amproc.amprocfamily = opf.oidand      am.amname = 'hash'order by opfamily_name,
         opfamily_procedure;
   opfamily_name    | opfamily_procedure
--------------------+-------------------- 
 abstime_ops        | hashint4
 aclitem_ops        | hash_aclitem
 array_ops          | hash_array
 bool_ops           | hashchar
...
```


尽管这些函数没有文档说明，但可以使用它们计算适当数据类型值的哈希码。例如，如果对«text_ops»运算符族使用«hashtext»函数：

```sql
demo=# select hashtext('one');
 hashtext  
-----------
 127722028
(1 row)
```

```sql
demo=# select hashtext('two');
 hashtext  
-----------
 345620034
(1 row)
```

### 属性


让我们看一下哈希索引的属性，其中此访问方法向系统提供有关自身的信息。上节我们提供了查询。现在，我们也不会超出上次的结果：

```sql
     name      | pg_indexam_has_property
---------------+-------------------------
 can_order     | f
 can_unique    | f
 can_multi_col | f
 can_exclude   | t
     name      | pg_index_has_property
---------------+-----------------------
 clusterable   | f
 index_scan    | t
 bitmap_scan   | t
 backward_scan | t
        name        | pg_index_column_has_property 
--------------------+------------------------------
 asc                | f
 desc               | f
 nulls_first        | f
 nulls_last         | f
 orderable          | f
 distance_orderable | f
 returnable         | f
 search_array       | f
 search_nulls       | f
```


哈希函数不保留顺序关系：如果一个键的哈希函数的值小于另一键的哈希函数的值，则无法对键本身如何排序做出结论。因此，通常哈希索引可以支持唯一的操作“=”：

```sql
demo=# select   opf.opfname AS opfamily_name,
         amop.amopopr::regoperator AS opfamily_operatorfrom     pg_am am,
         pg_opfamily opf,
         pg_amop amopwhere    opf.opfmethod = am.oidand      amop.amopfamily = opf.oidand      am.amname = 'hash'order by opfamily_name,
         opfamily_operator;
 opfamily_name |  opfamily_operator  
---------------+----------------------
 abstime_ops   | =(abstime,abstime)
 aclitem_ops   | =(aclitem,aclitem)
 array_ops     | =(anyarray,anyarray)
 bool_ops      | =(boolean,boolean)
...
```


因此，哈希索引不能返回有序数据（«can_order»，«orderable»）。出于同样的原因，哈希索引不能处理NULL：«equals»操作对NULL（«search_nulls»）没有意义。

由于哈希索引不存储键（仅存储其哈希码），因此它不能用于仅索引访问（«returnable»）。

此访问方法也不支持多列索引（«can_multi_col»）。

### 内部


从版本10开始，可以通过“ [pageinspect](https://postgrespro.com/docs/postgresql/10/pageinspect) ”扩展名查看哈希索引内部。这就是它的样子：

```sql
demo=# create extension pageinspect;
```


元页面（我们获取索引中的行数和已用的最大存储桶数）：

```sql
demo=# select hash_page_type(get_raw_page('flights_flight_no_idx',0));
 hash_page_type 
----------------
 metapage
(1 row)
```

```sql
demo=# select ntuples, maxbucketfrom hash_metapage_info(get_raw_page('flights_flight_no_idx',0));
 ntuples | maxbucket 
---------+-----------
   33121 |       127 
(1 row)
```


存储桶页面（我们获得活动元组和死元组的数量，即可以清除的元组的数量）：

```sql
demo=# select hash_page_type(get_raw_page('flights_flight_no_idx',1));
 hash_page_type
----------------
 bucket
(1 row)
```

```sql
demo=# select live_items, dead_itemsfrom hash_page_stats(get_raw_page('flights_flight_no_idx',1));
 live_items | dead_items
------------+------------
        407 |          0
(1 row)
```


等等。但是，在不检查源代码的情况下几乎不可能找出所有可用字段的含义。如果你想这样做，则应从[README](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/access/hash/README;hb=HEAD)开始。

## Btree

### 结构


B树索引类型，实现为«btree»访问方法，适用于可以排序的数据。换句话说，必须为数据类型定义«greater»、«greater or equal»、«less»、«less or equal»和«equal»运算符。请注意，相同的数据有时可以以不同的方式排序，这[使我们回到](https://habr.com/ru/company/postgrespro/blog/442546)了运算符族的概念。

与往常一样，B树的索引行被压缩到页面中。在叶子的页面中，这些行包含要建立索引的数据（键）和对表行的引用（TID）。在内部页面中，每一行都引用索引的子页面，并包含该页中的最小值。

B树具有一些重要的特征：

- B树是平衡的，也就是说，每个叶页面与根都由相同数量的内部页面分隔开。因此，搜索任何值都需要花费相同的时间。

- B树是多分支的，即每个页面（通常为8 KB）包含许多（数百个）TID。因此，B树的深度很小，对于非常大的表，实际上可以达到4–5的深度。

- 索引中的数据按非递减顺序排序（在页面之间和每个页面内部），并且同一级别的页面通过双向列表相互连接。因此，我们可以仅通过列表的一个方向或另一个方向获得有序数据集，而不必每次都返回到根。



下面是带整数键的一个字段上的索引的简化示例：

 ![img](indexs-in-postgresql/clip_image002-1572852375811.gif)

索引的第一页是一个元页，它引用索引根。内部节点位于根下方，叶子页面位于最底行。向下箭头表示从叶节点到表行（TID）的引用。

### 相等搜索


让我们考虑通过条件“ *indexed-field* = *expression**索引字段**=**表达式* ” 在树中搜索值。假设我们对49的键感兴趣。

 ![img](indexs-in-postgresql/clip_image004.gif)

搜索从根节点开始，我们需要确定要降到哪个子节点。由于知道根节点（4、32、64）中的键，因此我们可以计算出子节点中的值范围。由于32≤49 <64，所以我们需要下降到第二个子节点。接下来，递归重复相同的过程，直到我们到达一个叶节点，可以从该叶节点获得所需的TID。

实际上，许多细节使这个看似简单的过程变得复杂。例如，一个索引可以包含非唯一键，并且可能有太多相等的值以至于它们不适合一页。回到我们的示例，似乎我们应该从内部节点下降到参考值49。但是，从图中可以清楚地看出，通过这种方式，我们将跳过上一页中的«49»键中的一个。因此，一旦我们在内部页面中找到一个完全相等的键，就必须向左移一个位置，然后从左到右查看底层的索引行以寻找所需的键。

（另一个复杂之处在于，在搜索过程中，其他进程可以更改数据：可以重建树，可以将页面拆分为两个，依此类推。所有算法都针对这些并发操作而设计，不会互相干扰，也不会在任何可能的地方引起额外的锁定，但我们会避免在此基础上进行扩展。）

### 等值搜索


当通过条件“*indexed-field* ≤ *expression**索引字段* ≤ *表达式* ”（或"*indexed-field* ≥ *expression**索引字段* ≥ *表达式* “）进行搜索时，我们首先通过等式条件"*indexed-field* = *expression*" 在索引中找到一个值（如果有的话），然后按照适当的方向遍历叶页面直到结束。

该图说明了n≤35的过程：

 ![img](indexs-in-postgresql/clip_image006.gif)

 以类似的方式支持«greater»和«less»运算符，只是必须删除最初找到的值。

 

### 按范围搜索


通过范围 "*expression1* ≤ *indexed-field* ≤ *expression2* *表达式**1**≤**索引字段≤表达式2*"进行搜索时，通过条件"*indexed-field* = *expression1* *索引字段**=**表达式**1*"找到一个值，然后遍历叶页面直到"*indexed-field* ≤ *expression2* *索引字段**≤**表达式2*"被满足; 反之亦然：从第二个表达式开始，然后朝相反的方向走，直到到达第一个表达式。

该图显示了条件23≤n≤64的过程：

 ![img](indexs-in-postgresql/clip_image008.gif)

 

### 示例


让我们看一个查询计划的示例。像往常一样，我们使用演示数据库，这次我们将考虑aircraft表。它仅包含9行，规划器会选择不使用索引，因为整个表都被存放在一个页面。但是，为了说明目的，该表对我们来说很有趣。

```sql
demo=# select * from aircrafts;
 aircraft_code |        model        | range 
---------------+---------------------+-------
 773           | Boeing 777-300      | 11100
 763           | Boeing 767-300      |  7900
 SU9           | Sukhoi SuperJet-100 |  3000
 320           | Airbus A320-200     |  5700
 321           | Airbus A321-200     |  5600
 319           | Airbus A319-100     |  6700
 733           | Boeing 737-300      |  4200
 CN1           | Cessna 208 Caravan  |  1200
 CR2           | Bombardier CRJ-200  |  2700
(9 rows)

demo=# create index on aircrafts(range);

demo=# set enable_seqscan = off;
```

（或者明确地说，«在aircrafts表上使用btree（range）创建索引»，但是它是一个默认构建的B树。）

通过等式搜索：

```sql
demo=# explain(costs off) select * from aircrafts where range = 3000;
                    QUERY PLAN                     
---------------------------------------------------
 Index Scan using aircrafts_range_idx on aircrafts
   Index Cond: (range = 3000)
(2 rows)
```


 按不等式搜索：

 ```sql
demo=# explain(costs off) select * from aircrafts where range < 3000;
                    QUERY PLAN                    
---------------------------------------------------
 Index Scan using aircrafts_range_idx on aircrafts
   Index Cond: (range < 3000) 
(2 rows)
 ```


 按范围搜索：

```sql
demo=# explain(costs off) select * from aircrafts where range between 3000 and 5000;
                     QUERY PLAN                      
-----------------------------------------------------
 Index Scan using aircrafts_range_idx on aircrafts
   Index Cond: ((range >= 3000) AND (range <= 5000))
(2 rows)
```

### 排序


让我们再次强调一点，对于任何类型的扫描（索引，仅索引或位图），«btree»访问方法都会返回有序数据，我们可以在上图中清楚地看到。

因此，如果表在排序条件上具有索引，则优化器将考虑这两个选项：表的索引扫描（该索引扫描很容易返回排序后的数据）以及表的顺序扫描（随后对结果进行排序）。

#### 排序顺序


创建索引时，我们可以显式地指定排序顺序。例如，我们可以通过这种方式按照飞行距离创建索引：

```sql
demo=# create index on aircrafts(range desc);
```

在这种情况下，较大的值将出现在左侧的树中，而较小的值将出现在右侧。如果我们可以在任何方向上遍历索引值，为什么还需要这样做？

其目的是多列索引。让我们创建一个视图来显示aircraft（飞机）模型，传统的划分为短程，中程和远程飞机：

```sql
demo=# create view aircrafts_v as
select model,
       case
           when range < 4000 then 1
           when range < 10000 then 2
           else 3
       end as class
from aircrafts;

demo=# select * from aircrafts_v;
        model        | class
---------------------+-------
 Boeing 777-300      |     3
 Boeing 767-300      |     2
 Sukhoi SuperJet-100 |     1
 Airbus A320-200     |     2
 Airbus A321-200     |     2
 Airbus A319-100     |     2
 Boeing 737-300      |     2
 Cessna 208 Caravan  |     1
 Bombardier CRJ-200  |     1
(9 rows)
```


让我们创建一个索引（使用表达式）：

```sql
demo=# create index on aircrafts(
  (case when range < 4000 then 1 when range < 10000 then 2 else 3 end),
  model);
```


现在，我们可以使用该索引来获取对两列按升序排序的数据：

```sql
demo=# select class, model from aircrafts_v order by class, model;
 class |        model        
-------+---------------------
     1 | Bombardier CRJ-200
     1 | Cessna 208 Caravan
     1 | Sukhoi SuperJet-100
     2 | Airbus A319-100
     2 | Airbus A320-200
     2 | Airbus A321-200
     2 | Boeing 737-300
     2 | Boeing 767-300
     3 | Boeing 777-300
(9 rows)

demo=# explain(costs off)
select class, model from aircrafts_v order by class, model;
                       QUERY PLAN                       
--------------------------------------------------------
 Index Scan using aircrafts_case_model_idx on aircrafts
(1 row)
```


同样，我们可以执行查询以降序对数据进行排序：

```sql
demo=# select class, model from aircrafts_v order by class desc, model desc;
 class |        model        
-------+---------------------
     3 | Boeing 777-300
     2 | Boeing 767-300
     2 | Boeing 737-300
     2 | Airbus A321-200
     2 | Airbus A320-200
     2 | Airbus A319-100
     1 | Sukhoi SuperJet-100
     1 | Cessna 208 Caravan
     1 | Bombardier CRJ-200
(9 rows)

demo=# explain(costs off) select class, model from aircrafts_v order by class desc, model desc;
                           QUERY PLAN                            
-----------------------------------------------------------------
 Index Scan BACKWARD using aircrafts_case_model_idx on aircrafts
(1 row)
```


但是，我们不能使用此索引对一列降序，而对另一列升序来获取数据。这需要单独排序：

```sql
demo=# explain(costs off) select class, model from aircrafts_v order by class ASC, model DESC;
                   QUERY PLAN                    
-------------------------------------------------
 Sort
   Sort Key: (CASE ... END), aircrafts.model DESC
   ->  Seq Scan on aircrafts
(3 rows)

```

（请注意，作为最后的选择，计划器选择了顺序扫描，而不考虑之前的«enable_seqscan = off»设置。这是因为实际上此设置并不禁止表扫描，而只是设置其天文成本（个人理解是成本比较大的时候，上边的开关不起作用），请仔细阅读该计划 «costs on»。）

要使该查询使用索引，必须使用所需的排序方向来构建索引：

 ```sql
demo=# create index aircrafts_case_asc_model_desc_idx on aircrafts(
 (case
    when range < 4000 then 1
    when range < 10000 then 2
    else 3
  end) ASC,
  model DESC);

demo=# explain(costs off)
select class, model from aircrafts_v order by class ASC, model DESC;

                           QUERY PLAN                            
-----------------------------------------------------------------
 Index Scan using aircrafts_case_asc_model_desc_idx on aircrafts
(1 row)

 ```

#### 列顺序

使用多列索引时出现的另一个问题是索引中列出列的顺序。对于B树，此顺序非常重要：页面内的数据将按第一个字段排序，然后按第二个字段排序，以此类推。

我们可以用以下符号方式表示我们在范围区间和模型上构建的索引：

 ![img](indexs-in-postgresql/clip_image010.jpg)

实际上，这么小的索引肯定只适合一个根页面。在图中，为了清楚起见，它故意分布在几页中。

从此图表可以清楚地看出，通过像«class = 3»（仅在第一个字段中进行搜索）或«class = 3 and model = 'Boeing 777-300'»（在两个字段中进行搜索）这样的谓词进行搜索将有效地工作。

但是，根据谓词 «model = 'Boeing 777-300'» 进行搜索的效率会低得多：从根节点开始，我们无法确定要下降到哪个子节点，因此，我们必须下降到所有子节点。这并不意味着永远不能使用这样的索引——它的效率是有问题的。例如，如果我们有三类飞机，而每一类中都有很多模型，则我们将不得不查看大约三分之一的索引，这可能比全表扫描更有效……或者相反。

但是，如果我们创建这样的索引：

 ```sql
demo=# create index on aircrafts(
  model,
  (case when range < 4000 then 1 when range < 10000 then 2 else 3 end));
 ```


字段的顺序将发生变化：

 ![img](indexs-in-postgresql/clip_image012.jpg)

使用此索引，根据谓词 «model = 'Boeing 777-300'» 进行的搜索将有效地进行，但是根据谓词«class = 3»进行的搜索将无效。

#### 空值


 «btree»访问方法可以索引NULL，并支持按条件IS NULL和IS NOT NULL进行搜索。

 让我们考虑flights航班表，其中出现NULL：

```sql
demo=# create index on flights(actual_arrival);

demo=# explain(costs off) select * from flights where actual_arrival is null;
                      QUERY PLAN                       
-------------------------------------------------------
 Bitmap Heap Scan on flights
   Recheck Cond: (actual_arrival IS NULL)
   ->  Bitmap Index Scan on flights_actual_arrival_idx
         Index Cond: (actual_arrival IS NULL)
(4 rows)
```


NULL位于叶节点的一端或另一端，具体取决于创建索引的方式（NULLS FIRST或NULLS LAST）。如果查询包括排序，则这一点很重要：如果SELECT命令在其ORDER BY子句中指定的NULL顺序与构建索引指定的顺序相同（NULLS FIRST或NULLS LAST），则可以使用索引。

在下面的示例中，这些顺序相同，因此，我们可以使用索引：

```sql
demo=# explain(costs off)
select * from flights order by actual_arrival NULLS LAST;
                       QUERY PLAN                      
--------------------------------------------------------
 Index Scan using flights_actual_arrival_idx on flights
(1 row)
```


这些顺序是不同的，优化器选择顺序扫描和后续排序：

```sql
demo=# explain(costs off)
select * from flights order by actual_arrival NULLS FIRST;
               QUERY PLAN              
----------------------------------------
 Sort
   Sort Key: actual_arrival NULLS FIRST
   ->  Seq Scan on flights
(3 rows)
```


要使用索引，必须使用位于开头的NULL创建索引：

 ```sql
demo=# create index flights_nulls_first_idx on flights(actual_arrival NULLS FIRST);

demo=# explain(costs off)
select * from flights order by actual_arrival NULLS FIRST;
                     QUERY PLAN                      
-----------------------------------------------------
 Index Scan using flights_nulls_first_idx on flights
(1 row)
 ```


像这样的问题肯定是由NULL无法排序引起的，即NULL与其他任何值的比较结果均未定义：

```sql
demo=# \pset null NULL

demo=# select null < 42;
 ?column?
----------
 NULL
(1 row)
```


这与B树的概念背道而驰，不符合一般模式。但是，NULL在数据库中起着非常重要的作用，因此我们总是不得不为它们设置例外。

由于可以为NULL建立索引，因此即使对表没有施加任何条件，也可以使用索引（因为索引肯定包含所有表行上的信息）。如果查询需要数据排序并且索引确保了所需的顺序，则这样做很有意义。在这种情况下，计划者可以选择索引访问来保存单独的排序。

#### 属性


让我们看一下«btree»访问方法的属性（根据之前第二节提供的查询）。

```sql
 amname |     name      | pg_indexam_has_property
--------+---------------+-------------------------
 btree  | can_order     | t
 btree  | can_unique    | t
 btree  | can_multi_col | t
 btree  | can_exclude   | t
```


如我们所见，B树可以排序数据并支持唯一性——这是为我们提供此类属性的唯一访问方法。也允许使用多列索引，但是其他访问方法（尽管不是全部）都可以支持此类索引。下次，我们将讨论EXCLUDE约束的支持，这不是没有原因的。

```sql
     name      | pg_index_has_property
---------------+-----------------------
 clusterable   | t
 index_scan    | t
 bitmap_scan   | t
 backward_scan | t
```


«btree»访问方法支持两种获取值的技术：索引扫描和位图扫描。正如我们所看到的，访问方法既可以“向前”«forward»又可以“向后”«backward»遍历树。

```sql
        name        | pg_index_column_has_property 
--------------------+------------------------------
 asc                | t
 desc               | f
 nulls_first        | f
 nulls_last         | t
 orderable          | t
 distance_orderable | f
 returnable         | t
 search_array       | t
 search_nulls       | t
```


该层的前四个属性说明特定列的值如何精确排序。在此示例中，值以升序（«asc»）排序，最后提供NULL（«nulls_last»）。但是正如我们已经看到的，其他组合也是可能的。

 «search_array»属性通过索引支持这样的表达式：

```sql
demo=# explain(costs off)
select * from aircrafts where aircraft_code in ('733','763','773');
                           QUERY PLAN                            
-----------------------------------------------------------------
 Index Scan using aircrafts_pkey on aircrafts
   Index Cond: (aircraft_code = ANY ('{733,763,773}'::bpchar[]))
(2 rows)
```


 «returnable»属性表示支持仅索引扫描，这是合理的，因为索引行本身存储索引值（与哈希索引不同）。在这里，有必要谈谈关于覆盖基于B树的索引。

####  具有附加行的唯一索引 


正如我们[前面](https://habr.com/ru/company/postgrespro/blog/441962/)所讨论[的](https://habr.com/ru/company/postgrespro/blog/441962/)，覆盖索引是存储查询所需的所有值的索引，（几乎）不需要访问表本身。唯一索引可以专门覆盖。

但是，假设我们要向唯一索引添加查询所需的额外列。新的组合键的值现在可能不是唯一的，因此将需要在同一列上使用两个索引：一个唯一的（用于支持完整性约束）和另一个非唯一的（用于覆盖）。这肯定是低效的。

在我们公司的Anastasiya Lubennikova改进的«btree»方法，可以在唯一索引中包含其他非唯一的列。我们希望该补丁能被社区所采用，成为PostgreSQL的一部分，但是这不会在版本10中实现。目前，该补丁在Pro Standard 9.5+中可用，这就是它的样子。

实际上，此补丁已提交给PostgreSQL 11。


让我们考虑bookings预订表：

```sql
demo=# \d bookings
              Table "bookings.bookings"
    Column    |           Type           | Modifiers
--------------+--------------------------+-----------
 book_ref     | character(6)             | not null
 book_date    | timestamp with time zone | not null
 total_amount | numeric(10,2)            | not null
Indexes:
    "bookings_pkey" PRIMARY KEY, btree (book_ref)
Referenced by:
    TABLE "tickets" CONSTRAINT "tickets_book_ref_fkey" FOREIGN KEY (book_ref) REFERENCES bookings(book_ref)
```


在此表中，主键（book_ref，booking code预订代码）由常规的«btree»索引提供。让我们创建一个带有附加列的新唯一索引：

```sql
demo=# create unique index bookings_pkey2 on bookings(book_ref) INCLUDE (book_date);
```


现在，我们用一个新的索引替换现有的索引（在事务中，同时应用所有更改）：

```sql
demo=# begin;

demo=# alter table bookings drop constraint bookings_pkey cascade;

demo=# alter table bookings add primary key using index bookings_pkey2;

demo=# alter table tickets add foreign key (book_ref) references bookings (book_ref);

demo=# commit;
```


这是我们得到的：

```sql
demo=# \d bookings
              Table "bookings.bookings"
    Column    |           Type           | Modifiers
--------------+--------------------------+-----------
 book_ref     | character(6)             | not null
 book_date    | timestamp with time zone | not null
 total_amount | numeric(10,2)            | not null
Indexes:
    "bookings_pkey2" PRIMARY KEY, btree (book_ref) INCLUDE (book_date)
Referenced by:
    TABLE "tickets" CONSTRAINT "tickets_book_ref_fkey" FOREIGN KEY (book_ref) REFERENCES bookings(book_ref)
```


现在，一个相同的索引可以作为唯一索引，并充当此查询的覆盖索引，例如：

 ```sql
demo=# explain(costs off)
select book_ref, book_date from bookings where book_ref = '059FC4';
                    QUERY PLAN                    
--------------------------------------------------
 Index Only Scan using bookings_pkey2 on bookings
   Index Cond: (book_ref = '059FC4'::bpchar)
(2 rows)
 ```

#### 创建索引


众所周知，同样重要的是，对于大型表，最好在没有索引的情况下加载数据，然后再创建所需的索引。这样不仅速度更快，索引的大小也更小。

事实是，«btree»索引的创建比将值按行插入树中使用的过程更有效。大致上，对表中所有可用的数据进行排序，并创建这些数据的叶页面。然后将内部页面构建在此基础之上，直到整个金字塔收敛到根为止。

此过程的速度取决于可用RAM的大小，该大小受«maintenance_work_mem»参数的限制。因此，增加参数值可以加快此过程。对于唯一索引，除了«maintenance_work_mem»之外，还分配了大小为 «work_mem»的内存。

##### 比较语义

[上次](https://habr.com/ru/company/postgrespro/blog/442546/)我们提到PostgreSQL需要知道调用哪些哈希函数来获取不同类型的值，并且该关联存储在«hash»访问方法中。同样，系统必须弄清楚如何对值进行排序。这对于排序，分组（有时），合并联接等都是必需的。PostgreSQL不会绑定运算符名称（例如>，<，=），因为用户可以定义自己的数据类型并为相应的运算符赋予不同的名称。«btree»访问方法使用的运算符族定义了运算符名称。

例如，这些比较运算符用于«bool_ops»运算符族：

```sql
postgres=# select   amop.amopopr::regoperator as opfamily_operator,
         amop.amopstrategy
from     pg_am am,
         pg_opfamily opf,
         pg_amop amop
where    opf.opfmethod = am.oid
and      amop.amopfamily = opf.oid
and      am.amname = 'btree'
and      opf.opfname = 'bool_ops'
order by amopstrategy;
  opfamily_operator  | amopstrategy
---------------------+-------------- 
 <(boolean,boolean)  |            1
 <=(boolean,boolean) |            2
 =(boolean,boolean)  |            3
 >=(boolean,boolean) |            4
 >(boolean,boolean)  |            5
(5 rows) 
```




 在这里我们可以看到五个比较运算符，但是正如已经提到的，我们不应该依赖它们的名称。为了弄清楚每个运算符做了哪些比较，引入了策略概念。定义了五种策略来描述运算符语义：

- 1——小于
- 2——小于或等于

- 3——等于

- 4——大于或等于

- 5——大于



一些运算符族可以包含多个运算符实现一项策略。例如，«integer_ops»运算符族包含策略1的以下运算符：

```sql
postgres=# select   amop.amopopr::regoperator as opfamily_operator
from     pg_am am,
         pg_opfamily opf,
         pg_amop amop
where    opf.opfmethod = am.oid
and      amop.amopfamily = opf.oid
and      am.amname = 'btree'
and      opf.opfname = 'integer_ops'
and      amop.amopstrategy = 1
order by opfamily_operator;
  opfamily_operator  
---------------------- 
 <(integer,bigint)
 <(smallint,smallint)
 <(integer,integer)
 <(bigint,bigint)
 <(bigint,integer)
 <(smallint,integer)
 <(integer,smallint)
 <(smallint,bigint)
 <(bigint,smallint)
(9 rows) 
```


因此，当比较一个运算符系列中包含的不同类型的值时，优化器可以避免强制类型转换。

##### 对新数据类型的索引支持


该文档提供[了](https://postgrespro.com/docs/postgrespro/9.6/xindex)创建用于复数的新数据类型以及用于对该类型的值进行排序的运算符类[的示例](https://postgrespro.com/docs/postgrespro/9.6/xindex)。本示例使用C语言，当速度至关重要时，这是完全合理的。但是，为了更好地理解比较语义，我们可以在同一实验中使用纯SQL。

让我们用两个字段创建一个新的复合类型：实部和虚部。

```sql
postgres=# create type complex as (re float, im float);
```


我们可以创建一个具有新类型字段的表，并向该表中添加一些值：

```sql
postgres=# create table numbers(x complex);

postgres=# insert into numbers values ((0.0, 10.0)), ((1.0, 3.0)), ((1.0, 1.0));
```


 现在出现了一个问题：如果在数学意义上没有为复数定义任何顺序关系，该如何对它们进行排序？

 事实证明，已经为我们定义了比较运算符：

```sql
postgres=# select * from numbers order by x;
   x    
--------
 (0,10)
 (1,1)
 (1,3)
(3 rows)
```


默认情况下，对于复合类型，排序是按组件方式进行的：首先比较第一个字段，然后比较第二个字段，依此类推，与逐个字符比较文本字符串的方式大致相同。但是我们可以定义不同的顺序。例如，复数可被视为向量，并按模（长度）排序，模的计算是坐标平方和的平方根（毕达哥拉斯定理）。为了定义这样的顺序，让我们创建一个辅助函数来计算模量：

```sql
postgres=# create function modulus(a complex) returns float as $$
    select sqrt(a.re*a.re + a.im*a.im);
$$ immutable language sql;
```


现在，我们将使用此辅助函数系统地定义所有五个比较运算符的函数：

```sql
postgres=# create function complex_lt(a complex, b complex) returns boolean as $$
    select modulus(a) < modulus(b);
$$ immutable language sql;

postgres=# create function complex_le(a complex, b complex) returns boolean as $$
    select modulus(a) <= modulus(b);
$$ immutable language sql;

postgres=# create function complex_eq(a complex, b complex) returns boolean as $$
    select modulus(a) = modulus(b);
$$ immutable language sql;

postgres=# create function complex_ge(a complex, b complex) returns boolean as $$
    select modulus(a) >= modulus(b);
$$ immutable language sql;

postgres=# create function complex_gt(a complex, b complex) returns boolean as $$
    select modulus(a) > modulus(b);
$$ immutable language sql;
```


我们将创建相应的运算符。为了说明它们不需要被称为“>”，“ <”等，让我们给它们加上«weird»奇怪的名称。

```sql
postgres=# create operator #<#(leftarg=complex, rightarg=complex, procedure=complex_lt);

postgres=# create operator #<=#(leftarg=complex, rightarg=complex, procedure=complex_le);

postgres=# create operator #=#(leftarg=complex, rightarg=complex, procedure=complex_eq);

postgres=# create operator #>=#(leftarg=complex, rightarg=complex, procedure=complex_ge);

postgres=# create operator #>#(leftarg=complex, rightarg=complex, procedure=complex_gt);
```


在这一点上，我们可以比较数字：

```sql
postgres=# select (1.0,1.0)::complex #<# (1.0,3.0)::complex;
 ?column?
----------
 t
(1 row)
```


除了五个运算符之外，«btree»访问方法还需要定义一个函：如果第一个值小于，等于或大于第二个，那么它必须返回-1、0或1。此辅助功能称为support支持。其他访问方法可能需要定义其他支持函数。

```sql
postgres=# create function complex_cmp(a complex, b complex) returns integer as $$
    select case when modulus(a) < modulus(b) then -1
                when modulus(a) > modulus(b) then 1 
                else 0
           end;
$$ language sql;
```


现在，我们准备创建一个运算符类（自动创建同名运算符族）：

 ```sql
postgres=# create operator class complex_ops
default for type complex
using btree as
    operator 1 #<#,
    operator 2 #<=#,
    operator 3 #=#,
    operator 4 #>=#,
    operator 5 #>#,
    function 1 complex_cmp(complex,complex);
 ```


现在可以按需要进行排序：

 ```sql
postgres=# select * from numbers order by x;
   x    
--------
 (1,1)
 (1,3)
 (0,10)
(3 rows)
 ```


肯定会得到«btree»索引的支持。

要完成图片，您可以使用以下查询获取支持函数：

```sql
postgres=# select amp.amprocnum,
       amp.amproc,
       amp.amproclefttype::regtype,
       amp.amprocrighttype::regtype
from   pg_opfamily opf,
       pg_am am,
       pg_amproc amp
where  opf.opfname = 'complex_ops'
and    opf.opfmethod = am.oid
and    am.amname = 'btree'
and    amp.amprocfamily = opf.oid;
 amprocnum |   amproc    | amproclefttype | amprocrighttype
-----------+-------------+----------------+-----------------
         1 | complex_cmp | complex        | complex
(1 row)
```

#### 内部结构


我们可以使用«pageinspect»扩展来探索B树的内部结构。

```sql
demo=# create extension pageinspect;
```


索引元页：

```sql
demo=# select * from bt_metap('ticket_flights_pkey');
 magic  | version | root | level | fastroot | fastlevel
--------+---------+------+-------+----------+-----------
 340322 |       2 |  164 |     2 |      164 |         2
(1 row)
```


这里最有趣的是索引级别：一百万行的表在两列上的索引最少需要2个级别（不包括根）。

有关块164的统计信息（根）：

```sql
demo=# select type, live_items, dead_items, avg_item_size, page_size, free_size
from bt_page_stats('ticket_flights_pkey',164);
 type | live_items | dead_items | avg_item_size | page_size | free_size
------+------------+------------+---------------+-----------+-----------
 r    |         33 |          0 |            31 |      8192 |      6984
(1 row)
```


块中的据（这里牺牲了屏幕宽度的«data»字段，包含二进制表示的索引键的值）：

```sql
demo=# select itemoffset, ctid, itemlen, left(data,56) as data
from bt_page_items('ticket_flights_pkey',164) limit 5;
 itemoffset |  ctid   | itemlen |                           data                           
------------+---------+---------+----------------------------------------------------------
          1 | (3,1)   |       8 |
          2 | (163,1) |      32 | 1d 30 30 30 35 34 33 32 33 30 35 37 37 31 00 00 ff 5f 00
          3 | (323,1) |      32 | 1d 30 30 30 35 34 33 32 34 32 33 36 36 32 00 00 4f 78 00
          4 | (482,1) |      32 | 1d 30 30 30 35 34 33 32 35 33 30 38 39 33 00 00 4d 1e 00
          5 | (641,1) |      32 | 1d 30 30 30 35 34 33 32 36 35 35 37 38 35 00 00 2b 09 00
(5 rows)
```


第一个元素与技术有关，并指定块中所有元素的上限（我们未讨论的实现细节），而数据本身以第二个元素开头。显然，最左边的子节点是块163，然后是块323，依此类推。反过来，可以使用相同的函数进行探索。

现在，遵循一个良好的传统，阅读文档，[README](https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/access/nbtree/README;hb=HEAD)（https://git.postgresql.org/gitweb/?p=postgresql.git;a=blob;f=src/backend/access/nbtree/README;hb=HEAD）和源代码。

 还有一个更有用的扩展是“ [amcheck](https://postgrespro.com/docs/postgresql/10/amcheck) ”，它将整合到PostgreSQL 10中，对于较低版本，可以从[github](https://github.com/petergeoghegan/amcheck)（https://github.com/petergeoghegan/amcheck）获得。此扩展检查B树中数据的逻辑一致性，并使我们能够提前检测到故障。

确实如此，«amcheck»是PostgreSQL从版本10开始的一部分。
