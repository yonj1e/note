## 简介
2017 年 10 月 20 日，苏宁云商IT总部资深技术经理陈华军在“[PostgreSQL](https://cloud.tencent.com/product/postgresql?from=10680) 2017中国技术大会”进行《苏宁citus分布式数据库应用实践》演讲分享。

## 摘要

本次分享主要介绍了如何通过Citus打造分布式数据库，对具体的部署情况进行了讲解。

嘉宾演讲视频回放及PPT，请复制链接：http://t.cn/RdmlKXd**，粘贴至浏览器地址栏即可。

## 业务场景

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\cb852aeace7a4cc2bc4763edcad6e188\9dcaeefa3e8e41abad6a4c85356b7e82.jpeg)

上图的系统架构主要是做订单的分析，它会定时的从其他的业务系统中抽取订单以及订单的更新信息。每5分钟进行一次批量的处理，更新10张左右的明细表。在数据库中同样也是5分钟做一次处理，首先会对明细表进行计算，之后的计算结果会被放到报表中。架构外层还有一些其他系统，比如**cognos、智能分析**等，它们主要是用来从数据中查报表或明细表。

这套系统中我们采用的数据库是DB 2，平时的CPU负载都达到了50%左右，大促期间更是超过了80%，可以算是不堪重负。

#### DB负载在哪？

如此高的负载，到底问题是出在那些地方？其实主要是在**明细更新、报表计算、报表查询/明细查询上**。

**明细更新**时是5分钟更新10张明细表，这其中最宽的表有400字段，大概每行2.5kB。每次更新最宽的表约10w记录，总体上是30w。我们还要保持最近数天的数据。这样看下来其实主要的压力是在随机更新，换算一下大概每秒要做5k条记录的更新，关键是这 5K条记录还都是宽表。

**报表计算**也是每5分钟计算30多张报表，要求2分钟完成，每个报表平均执行14次明细表集合查询。估算下来大概是每分钟200次明细表的聚合运算。

**报表查询/明细查询**中要求的并发度是大于30，但正常情况下没有这么高，大概只有10个左右。同时要求的响应时间要小于3秒。

由于我们的系统接入的业务需要扩张，预计年内负载还会增加10倍，也就是说原先的每秒5k的明细表随机更新和3000w明细表数据，将提升为每秒5k的明细表随机更新和3亿明细表数据。

这样的背景下基于单机的DB 2肯定是搞不定的，我们需要的应该是一种分布式方案。

#### 方案选型

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\da25f284e9fd4368beca4fba17a61429\7f88070d77f14023851c9968e19bb310.jpeg)

上图列出的就是我们当时所考察的各种方案，因为PG在分析上还是比较有优势，所以这些方案都和PG相关。**第一个Greenplum**由于已经比较成熟了，所以我们一开始就比较看好，但是它更新慢、并发低的缺陷，不符合明细更新的性能要求，因此被排除在外。**第二个postgres_fdw**由于不支持聚合下推和并行查询，所以不符合明细表查询性能要求。**第三个PG_XL方案**我们并没有做深入的评估，但是GMT对性能是有影响的，估计很难满足我们对随机更新的需求。最后的citus的优势在于它是一个扩展，稳定性和可维护性都比较好，同时分片表的管理也很方便，最终我们选择的就是这个方案。

## Citus介绍

#### Citus架构与原理

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\61341a80e80d4ba7851907652aba2eaf\5efe536adff04aca916ec1876fa519cd.jpeg)

这张是Citus的架构图，可以看到它由**1个maste**和**多个worker**组成，数据表通过hash或者append分片的方式分散到每个worker上。这里的表被分为分片表和参考表，参考表只有一个分片，并且每个worker上都有一份。

在应用访问的时候master接收应用的查询SQL，然后对查询SQL进行解析生成分布式执行计划，再将子执行路径分发到worker上执行，最后汇总执行结果返回给应用。

Citus主要适用于两种环境，一种是实时数据分析，一种是多租户应用。

#### 案例演示

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\7050765b3f744c42a898f11d404926f8\5d4a638693774bd4bfdb630ab4bb6e9b.jpeg)

这里演示的是Citus的使用过程。分片表的创建和普通表是一样的，只不过完成之后需要设置分片数，最后执行create_distributed_table函数，参数为需要分片的表以及分片字段，还可以指定分片方法，默认是hash方式。参考表的不同在于函数换成了create_reference_table。这两个函数主要做了两件事，首先是在每个worker上创建分片，其次是更新元数据。元数据定义了分片信息。

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\980fa4c3f42d4a85906334eb406d8b37\549d35485cf8478092cfd140032e1a37.jpeg)

元数据pg_dist_partition中存放的是分片表和分片规则，可以从图中看到，h代表的hash分片，n表示的是参考表。分片表中有一个partkey，它用来指定哪个字段做分片以及分片类型。

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\26fa2d1a9f2c46cea95a230a74476cde\c910eac6f40042cd9d45191a473e0702.jpeg)

元数据- pg_dist_shard定义了每个分片以及分片对应的hash范围，不过参考表由于只有一个分片，所以没有hash范围。

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\e574e61930fb42ca9dc5cc6d5885189b\eeaf4c9aeb9743daab6ad80388ce4fb5.jpeg)

元数据-pg_dist_shard_placement定义了每个分片存放的位置，第一列是分片的ID号，后面是所在的worker节点位置和端口号。

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\e8c31d48d89a44ea9556ce210532b738\95aa65cd075b44d994344ed9708bfd97.jpeg)

基于元数据master可以生成分布式执行计划，比如聚合查询就会生成如上图所示的执行计划。上半部分是在每个worker上预聚合，每个分片并行执行，下面则是master对worker的结果做最终的聚合。

## SQL限制

#### SQL限制—查询

Citus最大的缺陷在于有着SQL限制，并不是所有SQL都支持。最典型的就是对Join的限制，它不支持2个非亲和分片表的outer join，仅task-tracker执行器支持2个非亲和分片表的inner join，对分片表和参考表的outer join，参考表只能出现在left join的右边或right join的左边。对子查询也有着限制，子查询不能参与join，不能出现order by，limit和offset。一些SQL特性Citus同样不支持，比如CTE、Window函数、集合操作、非分片列的count(distinct)。最后还有一点需要注意，即本地表不能和分片表(参考表)混用。

这些限制其实都可以使用某些方法绕过，比如通过Hll(HyperLogLog)插件支持count(distinct)，对于其他的一些操作也可以通过临时表或dblink中转。不过临时表的问题在于会将一个SQL拆成多个SQL。


#### SQL限制—更新

在更新上也存在一些限制，它不支持跨分片的更新SQL和事务，‘insert into ... select ... from ...’的支持存在部分限制，插入源表和目的表必须是具有亲和性的分片表，不允许出现Stable and volatile函数，不支持LIMIT，OFFSET，窗口函数，集合操作，Grouping sets，DISTINCT。

当然这些限制也存在对应的回避方法，首先是使用copy代替insert，其次是用SELECT master_modify_multiple_shards(‘…’)实现扩分片更新。

#### SQL限制—DDL

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\3814afc1de6648649c2fc1f7eb026228\e9121fe4b1934dd49d7eb5b4563e3300.jpeg)

上图展示的是对DDL的支持情况，这里面大部分都是支持的，对于不支持的可以通过创建对等的唯一索引代替变更主键，或者使用`run_command_on_placements`函数，直接在所有分片位置上执行DDL的方式来进行回避。

## 两种执行器

Citus有两种执行器，通过**set citus.task_executor_type='task-tracker'|'real-time'**进行切换。

默认的real-time又分为router和非router方式。router适用于只需在一个shard上执行的SQL，1个master后端进程对每个worker只创建一个连接，并缓存连接。非route下master后端进程会对所有worker上的所有shard同时发起连接，并执行SQL，SQL完成后断开连接。

如果使用task-tracker执行器。Master是只和worker上的task-tracker进程交互，task-tracker进程负责worker上的任务调度，任务结束后master从worker上取回结果。worker上总的并发任务数可以通过参数控制。

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\ef5bcf14c18c408f965ed43eb387296d\25790bbc4deb44b0ac89ef554e13f91f.jpeg)

这里对这两种执行器进行了比较。real-time的优势主要在于响应时间小。task-tracker则是支持数据重分布，SQL支持也比real-time略好，同时并发数，资源消耗可控。

## 痛点

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\68b06d92701a4510af7e137a294205d3\24744a0468424e869f8b1a118863ce06.jpeg)

我们的系统中首先面临的痛点就是对随机更新速度要求高。上图左边是Citus官方展示的性能数据，看似接近所需的性能要求，实际上远远不够，因为这里记录的是普通的窄表，而我们的是宽表而且还有其他的负载。

图中右边是我这边做的性能测试。单机状态下插入速度是每秒13万条，使用Citus后下降到了5w多，这主要是由于master要对SQL进行解析和分发。在尝试对Citus进行优化后，使Citus不解析SQL，提升也不是很明显。最后一种方式是不使用master，将每个worker作为master，这次的效果达到了每秒30万条。

第二个痛点就是前面提到的SQL限制问题，虽然这些限制都有方法回避，但是对应用的改造量比较大。

#### 解决方案

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\7fe5d94bce844699ab7a3141c7ab16aa\2159476f0f4f446f960ad43c1fed4d6d.jpeg)

这是我们最终的解决方案。首先对于插入和更新数据慢的问题，不在走master，直接在worker上更新。在更新之前会现在worker上查询分片的元数据，然后再进行更新。

另外为了尽量减少SQL限制对应用的影响，我们采用的策略是尽量少做分片，只对明细表进行分片。应用在查询的时候会将报表和维表做join，也会将明细表和维表做join，那么这里就会出现问题，因为本地表和参考表不能出现在同一个SQL里。所以我们做了N份参考表，每个worker放一份，同时再将一份本地维表放在master上，由报表做join用，最后在更新的时候通过触发器同步本地维表和参考表。

#### 辅助工具函数开发

为了支撑前面提到的两个策略，我们实现了两个函数。**pg_get_dist_shard_placement()函数**用来批量获取记录所在分片位置函数。**create_sync_trigger_for_table()**函数用来自动生成本地维表和参考维表同步触发器的函数。

## 连接池

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\e0ad293d10e94e019b42ef644124ddd9\3f6c9fc343954a1e8dc3ca909d6796db.jpeg)

因为业务对SQL的响应时间要求较高，所以我们使用的是real time执行器。但是由于real time存在的缺陷，因此我们在master上部署了两套pgbounce连接池。一个在PostgreSQL前面，应用在连接PostgreSQL前先连接到pgbouncer。另一个在master和worker之间。

实际的使用的时候由于pgbounce不支持prepare语句，所以有些应用还是要直连到master。

效果

![img](C:\Users\yangjie11\AppData\Local\YNote\data\yonj1e@163.com\53aad39630b346cb9f79f69bb402bd6f\1ba02c7890d94dffb8b6efb17e7f1929.jpeg)

上图是POC压测的结果，基本上明细更新和报表结算满足了性能要求。测试的时候我们使用的是8个worker，而在部署的时候其实是先部署4台，然后再扩容到8台。

## 日常维护

Citus的维护和普通的PG维护在大部分情况下区别不大，不过有些有时候DDL执行会无法分发，这时可以用它的一些公有函数来完成。

另外更新多副本分片表的途中worker发生故障，可能导致该worker上的副本没有被正确更新。此时citus会在系统表pg_dist_shard_placement 中将其标识为“失效”状态。使用master_copy_shard_placement() 函数就能够进行恢复。

Citus对DDL、copy等跨库操作采用2PC保障事务一致，2PC中途发生故障会产生未决事务。对每个2PC事务中的操作都记录到系统表pg_dist_transaction，通过该表就能够判断哪些事务该回滚或提交。

## 踩过的坑

在实际的应用中我们并没有碰到什么大坑，主要是一些小问题。

**第一个是由于master(real-time)到worker用的短连接**，pgbouncer默认记录连接和断连接事件，导致日志文件增长太快。后来我们将其关闭了。

**第二个是master(real-time)会瞬间创建大量到worker 的并发连接**，而默认的unix套接字的 backlog连接数偏低， master节点的 PostgreSQL日志中经常发现大量连接出错的告警。对此的解决办法是修改修改pgbouncer的listen_backlog，然后硬重启pgbouncer。

以上为今天的全部分享内容，谢谢大家！

**注:本文内容基于较早的citus 6.x版，当前版本citus中“master”节点的名称已改为“Coordinator”。**

## 参考

https://pan.baidu.com/s/1_tkfp9xLSQuwA2LEInQWTw

- Sharding-JDBC：分布式微服务数据库访问框架的设计与实现
- 你是否知道怎样借助ES在不同场景下构建数据仓库
- 分布式强一致性数据库的灵魂 - Raft 算法